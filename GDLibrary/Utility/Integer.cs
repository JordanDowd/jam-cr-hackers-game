﻿/* Used to box values to send to EventData in the addditionalEventParameters parameter. 
 * Note how we code so that we can cast to and from this type */
  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Utility
{
    public class Integer
    {
        public static Integer Zero = new Integer(0);
        public static Integer One = new Integer(1);

        private int value;

        public int Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
            }
        }

        public Integer(int value)
        {
            this.value = value;
        }

        public override string ToString()
        {
            return "Value: " + this.value;
        }

        public static Integer operator *(Integer x, int multiplier)
        {
            return new Utility.Integer(x.Value * multiplier);
        }

        public static Integer operator *(int multiplier, Integer x)
        {
            return new Utility.Integer(multiplier * x.Value);
        }

        public static implicit operator Integer(int x)
        {
            return new Integer(x);
        }

        public static implicit operator int(Integer x)
        {
            return x.Value;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }   
}
