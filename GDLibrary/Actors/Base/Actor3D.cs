﻿using GDLibrary.Actors.Base;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Parameters.Transforms;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Actors
{
    public class Actor3D : Actor, ICloneable
    {
        #region Variables
        private Transform3D transform;
        #endregion

        #region Properties
        public Transform3D Transform3D
        {
            get
            {
                return this.transform;
            }

            set
            {
                this.transform = value;
            }
        }

        #endregion
        public Actor3D(string id, ActorType actorType, 
            Transform3D transform, StatusType statusType)
            : base(id, actorType, statusType)
        {
            this.transform = transform;
        }

        //return the world matrix that will scale rotate and place
        //the actor in the 3D world
        public override Matrix GetWorldMatrix()
        {
            return this.transform.World;
        }

        public virtual void Draw(GameTime gameTime)
        {

        }

        public override bool Equals(object obj)
        {
            Actor3D other = obj as Actor3D;

            if (other == null)
                return false;
            else if (this == other)
                return true;

            return this.Transform3D.Equals(other.Transform3D) && base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.Transform3D.GetHashCode();
            hash = hash * 17 + base.GetHashCode();
            return hash;
        }

        public new object Clone()
        {
            IActor actor = new Actor3D("clone - " + ID, //deep
                this.ActorType, //deep
                (Transform3D)this.transform.Clone(), //deep
                this.StatusType); //shallow

            //clone each of the (behavioural) controllers
            foreach (IController controller in this.ControllerList)
                actor.AttachController((IController)controller.Clone());

            return actor;
        }

        public override bool Remove()
        {
            //tag for garbage collection
            this.transform = null;
            return base.Remove();
        }
    }
}
