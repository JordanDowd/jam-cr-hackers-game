﻿/* Store, updating and drawing all visible objects in the game */
using GDLibrary.Actors;
using GDLibrary.Actors.Camera;
using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Actors.Drawn._3D.Collidable.Player.Animated;
using GDLibrary.Actors.Drawn._3D.Primitives;
using GDLibrary.Actors.Drawn._3D.Primitives.Billboards;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Interfaces;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Content;
using GDLibrary.Parameters.Effects;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Utility;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers.Object
{
    public class ObjectManager //: DrawableGameComponent
    {
        #region
        private Game game;
        private CameraManager cameraManager;
        private List<Actor3D> removeList, opaqueDrawList, transparentDrawList;
        private RasterizerState rasterizerStateOpaque;
        private RasterizerState rasterizerStateTransparent;

        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private InventoryManager inventoryManager;

        #endregion

        #region Properties
        public List<Actor3D> OpaqueDrawList
        {
            get
            {
                return this.opaqueDrawList;
            }
        }
        public List<Actor3D> TransparentDrawList
        {
            get
            {
                return this.transparentDrawList;
            }
        }
        #endregion

#if DEBUG
        //count the number of drawn objects to show frustum culling is happening - see DebugDrawer::Draw()
        private int debugDrawCount;

        public int DebugDrawCount
        {
            get
            {
                return this.debugDrawCount;
            }
        }
#endif

        public ObjectManager(Game game, CameraManager cameraManager, EventDispatcher eventDispatcher, int initialSize,
            ContentDictionary<Model> modelDictionary, ContentDictionary<Texture2D> textureDictionary, InventoryManager inventoryManager)
            //:base(game)
        {
            this.game = game;
            this.cameraManager = cameraManager;
            this.opaqueDrawList = new List<Actor3D>(initialSize);
            this.transparentDrawList = new List<Actor3D>(initialSize);
            this.removeList = new List<Actor3D>(initialSize);
            InitializeGraphics(game);

            this.modelDictionary = modelDictionary;
            this.textureDictionary = textureDictionary;
            this.inventoryManager = inventoryManager;
            //register with the event dispatcher for events of interest
            RegisterForEventHandling(eventDispatcher);
        }

        #region Event Handling
        protected virtual void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.OpacityChanged += EventDispatcher_OpacityChanged;
            eventDispatcher.RemoveActorChanged += EventDispatcher_RemoveActorChanged;
            eventDispatcher.PlaceObjectEvent += EventDispatcher_PlaceObjectEvent;
            eventDispatcher.ChangeTextureEvent += EventDispatcher_ChangeTextureEvent;
        }

        private void EventDispatcher_OpacityChanged(EventData eventData)
        {
            Actor3D actor = eventData.Sender as Actor3D;

            if(eventData.EventType == EventActionType.OnOpaqueToTransparent)
            {
                //remove from opaque list and add to transparent list
                this.opaqueDrawList.Remove(actor);
                this.transparentDrawList.Add(actor);
            }
            else if (eventData.EventType == EventActionType.OnTransparentToOpaque)
            {
                //remove from opaque list and add to transparent list
                this.transparentDrawList.Remove(actor);
                this.opaqueDrawList.Add(actor);
            }
        }

        private void EventDispatcher_RemoveActorChanged(EventData eventData)
        {
            if(eventData.EventType == EventActionType.OnRemoveActor)
            {
                this.Remove(eventData.Sender as Actor3D);
            }
        }

        private void EventDispatcher_PlaceObjectEvent(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnAddActor)
            {
                if (this.inventoryManager.InventoryContains("Magical Sword"))
                {
                    BasicEffect basicEffect = new BasicEffect(this.game.GraphicsDevice);

                    basicEffect.TextureEnabled = true;
                    basicEffect.PreferPerPixelLighting = true;
                    basicEffect.EnableDefaultLighting();

                    BasicEffectParameters effectParameters = new BasicEffectParameters(basicEffect);
                    effectParameters.Texture = this.textureDictionary["Sword"];

                    effectParameters.EmissiveColor = Color.White;
                    effectParameters.SpecularColor = Color.White;
                    effectParameters.SpecularPower = 1;
                    Transform3D transform3D = new Transform3D(new Vector3(86.1f, 18.8f, 10.3f),
                    new Vector3(90, -90, 0), new Vector3(0.1f, 0.1f, 0.1f), Vector3.UnitX, Vector3.UnitY);


                    ModelObject Sword = new ModelObject("Magical Sword", ActorType.Decorator,
                    transform3D, effectParameters, this.modelDictionary["Sword"]);

                    this.Add(Sword);

                    this.inventoryManager.RemoveFromInventory("Magical Sword");

                    TriangleMeshObject actor = (eventData.Sender as TriangleMeshObject);

                    actor.EffectParameters.Texture = this.textureDictionary["StatueGlow"];
                    actor.EffectParameters.DiffuseColor = Color.White;


                    //Change sound to swordPlace Sound
                    object[] additionalParameters1 = { "ButtonClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters1));

                }

            }
        }

        private void EventDispatcher_ChangeTextureEvent(EventData eventData)
        {
            TriangleMeshObject actor = (eventData.Sender as TriangleMeshObject);
            //string texture = eventData.AdditionalParameters[0] as string;

            actor.EffectParameters.Texture = this.textureDictionary["StatueGlow"];
            //actor.EffectParameters.DiffuseColor = Color.White;
        }

        #endregion


        private void InitializeGraphics(Game game)
        {
            //set the graphics to repeat pixel values
            //to fix obvious joint between textures
            SamplerState samplerState = new SamplerState();
            samplerState.AddressU = TextureAddressMode.Mirror;
            samplerState.AddressV = TextureAddressMode.Mirror;
            game.GraphicsDevice.SamplerStates[0] = samplerState;

            //enable alpha blending for transparent objects
            //game.GraphicsDevice.BlendState = BlendState.AlphaBlend;

            //opaque objects
            this.rasterizerStateOpaque = new RasterizerState();
            this.rasterizerStateOpaque.CullMode = CullMode.CullCounterClockwiseFace;

            //transparent objects
            //for all object in the game - EXPENSIVE - CAREFUL NOW!
            this.rasterizerStateTransparent = new RasterizerState();
            rasterizerStateTransparent.CullMode = CullMode.None;
        }

        private void SetGraphicsStateObjects(bool isOpaque)
        {
            //Remember this code from our initial aliasing problems with the Sky box?
            //enable anti-aliasing along the edges of the quad i.e. to remove jagged edges to the primitive
            game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            if (isOpaque)
            {
                //set the appropriate state for opaque objects
                game.GraphicsDevice.RasterizerState = this.rasterizerStateOpaque;

                //disable to see what happens when we disable depth buffering - look at the boxes
                game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            }
            else
            {
                //set the appropriate state for transparent objects
                game.GraphicsDevice.RasterizerState = this.rasterizerStateTransparent;

                //enable alpha blending for transparent objects i.e. trees
                game.GraphicsDevice.BlendState = BlendState.AlphaBlend;

                //disable to see what happens when we disable depth buffering - look at the boxes
                game.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            }
        }

        public void Add(Actor3D actor)
        {
            if (actor.GetAlpha() == 1)
            {
                this.opaqueDrawList.Add(actor);
            }
            else
            {
                this.transparentDrawList.Add(actor);
            }
        }

        public void Remove(Actor3D actor)
        {
            this.removeList.Add(actor);
        }

        public int Remove(Predicate<Actor3D> predicate)
        {
            List<Actor3D> resultList = null;

            resultList = this.opaqueDrawList.FindAll(predicate);
            if ((resultList != null) && (resultList.Count != 0)) //the actor(s) were found in the opaque list
            {
                foreach (Actor3D actor in resultList)
                    this.removeList.Add(actor);
            }
            else //the actor(s) were found in the transparent list
            {
                resultList = this.transparentDrawList.FindAll(predicate);

                if ((resultList != null) && (resultList.Count != 0))
                    foreach (Actor3D actor in resultList)
                        this.removeList.Add(actor);
            }

            return resultList != null ? resultList.Count : 0;
        }

        //batch remove on all objects that were requested to be removed
        protected virtual void ApplyRemove()
        {
            foreach (Actor3D actor in this.removeList)
            {
                if (actor.GetAlpha() == 1)
                    this.opaqueDrawList.Remove(actor);
                else
                    this.transparentDrawList.Remove(actor);
            }

            this.removeList.Clear();
        }

        public void Update(GameTime gameTime)
        {
            //remove any outstanding objects since the last update
            ApplyRemove();

            //update all your opaque objects
            foreach (Actor3D actor in this.opaqueDrawList)
            {
                if ((actor.GetStatusType() & StatusType.Update) != 0) //if update flag is set
                    actor.Update(gameTime);
            }

            //update all your transparent objects
            foreach (Actor3D actor in this.transparentDrawList)
            {
                if ((actor.GetStatusType() & StatusType.Update) != 0) //if update flag is set
                {
                    actor.Update(gameTime);
                    //used to sort objects by distance from the camera so that proper depth representation will be shown
                    MathUtility.SetDistanceFromCamera(actor as Actor3D, this.cameraManager.ActiveCamera);
                }
            }

            //sort so that the transparent objects closest to the camera are the LAST transparent objects drawn
            SortTransparentByDistance();
        }

        private void SortTransparentByDistance()
        {
            //sorting in descending order
            this.transparentDrawList.Sort((a, b) => (b.Transform3D.DistanceToCamera.CompareTo(a.Transform3D.DistanceToCamera)));
        }

        public void Draw(GameTime gameTime, Camera3D activeCamera)
        {
            //modify Draw() method to pass in the currently active camera - used to support multiple camera viewports - see ScreenManager::Draw()
            //set the viewport dimensions to the size defined by the active camera
            this.game.GraphicsDevice.Viewport = activeCamera.Viewport;

#if DEBUG
            //count the number of drawn objects to show frustum culling is happening - see DebugDrawer::Draw()
            this.debugDrawCount = 0;
#endif

            SetGraphicsStateObjects(true);
            foreach (Actor3D actor in this.opaqueDrawList)
            {
                DrawByType(gameTime, actor as Actor3D, activeCamera);
            }

            SetGraphicsStateObjects(false);
            foreach (Actor3D actor in this.transparentDrawList)
            {
                DrawByType(gameTime, actor as Actor3D, activeCamera);
            }
        }

        //calls the correct DrawObject() based on underlying object type
        private void DrawByType(GameTime gameTime, Actor3D actor, Camera3D activeCamera)
        {
            //was the drawn enum value set?
            if ((actor.StatusType & StatusType.Drawn) == StatusType.Drawn)
            {
                if (actor is AnimatedPlayerObject)
                {
                    DrawObject(gameTime, actor as AnimatedPlayerObject, activeCamera);
                }
                else if (actor is ModelObject)
                {
                    DrawObject(gameTime, actor as ModelObject, activeCamera);
                }
                //we will add additional else...if statements here to render other object types (e.g model, animated, billboard etc)
            }
        }

        //draw a NON-TEXTURED primitive i.e. vertices (and possibly indices) defined by the user
        private void DrawObject(GameTime gameTime, PrimitiveObject primitiveObject, Camera3D activeCamera)
        {
            if (activeCamera.BoundingFrustum.Intersects(primitiveObject.BoundingSphere))
            {
                primitiveObject.EffectParameters.SetParameters(activeCamera);
                primitiveObject.EffectParameters.SetWorld(primitiveObject.GetWorldMatrix());
                primitiveObject.VertexData.Draw(gameTime, primitiveObject.EffectParameters.Effect);

#if DEBUG
                debugDrawCount++;
#endif
            }
        }

        private void DrawObject(GameTime gameTime, BillboardPrimitiveObject billboardPrimitiveObject, Camera3D activeCamera)
        {
            if (activeCamera.BoundingFrustum.Intersects(billboardPrimitiveObject.BoundingSphere))
            {
                billboardPrimitiveObject.EffectParameters.SetParameters(activeCamera, billboardPrimitiveObject.BillboardOrientationParameters);
                billboardPrimitiveObject.EffectParameters.SetWorld(billboardPrimitiveObject.GetWorldMatrix());
                billboardPrimitiveObject.VertexData.Draw(gameTime, billboardPrimitiveObject.EffectParameters.Effect);

#if DEBUG
                debugDrawCount++;
#endif
            }
        }
        //draw a model object
        private void DrawObject(GameTime gameTime, ModelObject modelObject, Camera3D activeCamera)
        {
            if (activeCamera.BoundingFrustum.Intersects(modelObject.BoundingSphere))
            {
                if (modelObject.Model != null)
                {
                    modelObject.EffectParameters.SetParameters(activeCamera);

                    foreach (ModelMesh mesh in modelObject.Model.Meshes)
                    {
                        foreach (ModelMeshPart part in mesh.MeshParts)
                        {
                            part.Effect = modelObject.EffectParameters.Effect;
                        }
                        modelObject.EffectParameters.SetWorld(modelObject.BoneTransforms[mesh.ParentBone.Index]
                        * modelObject.GetWorldMatrix());
                        mesh.Draw();
                    }
                }
#if DEBUG
                debugDrawCount++;
#endif
            }
        }

        private void DrawObject(GameTime gameTime, AnimatedPlayerObject animatedPlayerObject, Camera3D activeCamera)
        {
            //an array of the current positions of the model meshes
            Matrix[] bones = animatedPlayerObject.AnimationPlayer.GetSkinTransforms();
            Matrix world = animatedPlayerObject.GetWorldMatrix();

            for (int i = 0; i < bones.Length; i++)
            {
                bones[i] *= world;
            }

            foreach (ModelMesh mesh in animatedPlayerObject.Model.Meshes)
            {
                foreach (SkinnedEffect skinnedEffect in mesh.Effects)
                {
                    skinnedEffect.SetBoneTransforms(bones);
                    skinnedEffect.View = activeCamera.View;
                    skinnedEffect.Projection = activeCamera.Projection;

                    //if you want to overwrite the texture you baked into the animation in 3DS Max then set your own texture
                    if (animatedPlayerObject.EffectParameters.Texture != null)
                        skinnedEffect.Texture = animatedPlayerObject.EffectParameters.Texture;

                    skinnedEffect.DiffuseColor = animatedPlayerObject.EffectParameters.DiffuseColor.ToVector3();
                    skinnedEffect.Alpha = animatedPlayerObject.Alpha;
                    skinnedEffect.EnableDefaultLighting();
                    skinnedEffect.PreferPerPixelLighting = true;
                }
                mesh.Draw();
            }

#if DEBUG
            debugDrawCount++;
#endif
        }
    }
}
