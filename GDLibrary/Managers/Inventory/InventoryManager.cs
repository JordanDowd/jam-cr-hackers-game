﻿using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Managers.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Managers
{
    public class InventoryManager
    {
      
        private List<string> inventory;
        private Dictionary<string, bool> itemsUsed;
        private Game game;
        public InventoryManager(Game game, EventDispatcher eventDispatcher)
        {
            this.game = game;
            this.inventory = new List<string>();
            this.itemsUsed = new Dictionary<string, bool>();
            RegisterForEventHandling(eventDispatcher);
     
        }

        public List<string> Inventory { get => inventory; set => inventory = value; }


        protected void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.InventoryEvent += EventDispatcher_InventoryEvent;
        }
        protected virtual void EventDispatcher_InventoryEvent(EventData eventData)
        {

            switch (eventData.EventType)
            {
                case Enums.EventActionType.AddToInventory:
                    AddToInventory(eventData);
                    break;
                case Enums.EventActionType.RemoveFromInventory:
                    RemoveFromInventory(eventData.ID);
                    break;
                default:
                    break;
            }

        }

        public void AddToInventory(EventData eventData)
        {

            if(!this.inventory.Contains(eventData.ID))
            {
                this.inventory.Add(eventData.ID);
                this.itemsUsed.Add(eventData.ID, false);

                if(eventData.ID == "Magical Sword")
                {
                    object[] additionalParameters = { "SwordPickup" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }
           
        }

        public void RemoveFromInventory(string itemID)
        {
            if (this.inventory.Contains(itemID))
            {
                this.inventory.Remove(itemID);
                this.itemsUsed[itemID] = true;

                if (itemID == "Magical Sword")
                {
                    object[] additionalParameters = { "SwordPlace" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }
 
        }

        public bool InventoryContains(string ID)
        {
            if (this.inventory.Contains(ID))
            {
                return true;
            }
            return false;
        }

        public string printInventory()
        {
            string inventoryItems = "";
         
            for (int i = 0; i < inventory.Count; i++)
            {
                inventoryItems += inventory[i] + ", \n";
            }
            return inventoryItems;
        }

        public bool ItemUsed(string ItemId)
        {
            if (this.itemsUsed.ContainsKey(ItemId))
            {

                return this.itemsUsed[ItemId];
            }
            return false;

        }

        public void CheckSize()
        {
            Console.WriteLine(itemsUsed.Count);
        }
    }
}
