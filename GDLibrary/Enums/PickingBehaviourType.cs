﻿/*Used by PickingManager to specify the behaviour required on picking (i.e. place or remove)*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Enums
{
    public enum PickingBehaviourType : sbyte
    {
        PickAndPlace,
        PickAndRemove
    }
}
