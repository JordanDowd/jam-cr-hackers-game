﻿using GDLibrary.Actors;
using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Controller.Base;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controllers.Camera3D.Base
{
    public class RingController : TargetController
    {
        #region Variables
        #endregion

        #region Properties
        #endregion

        public RingController(string id, ControllerType controllerType, IActor targetActor)
            : base(id, controllerType, targetActor)
        {
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            Vector3 offset = new Vector3(0, -2, 0);
            Actor3D parentActor = actor as Actor3D;
            DrawnActor3D targetDrawnActor = this.TargetActor as DrawnActor3D;

            if (targetDrawnActor != null)
            {
                parentActor.Transform3D.Translation = targetDrawnActor.Transform3D.Translation + offset;
            }
        }
    }
}
