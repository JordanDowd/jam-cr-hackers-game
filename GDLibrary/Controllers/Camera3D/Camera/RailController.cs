﻿using GDLibrary.Actors;
using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Controller.Base;
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using GDLibrary.Parameters.Camera;
using GDLibrary.Utility;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controller.Camera3D
{
    public class RailController : TargetController
    {
        #region Variables
        private RailParameters railParameters;
        private bool bFirstUpdate = true;
        #endregion

        #region Properties
        public RailParameters RailParameters
        {
            get
            {
                return this.railParameters;
            }
            set
            {
                this.railParameters = value;
            }
        }

        #endregion

        public RailController(string id, ControllerType controllerType, IActor targetActor, RailParameters railParameters)
            : base(id, controllerType, targetActor)
        {
            this.railParameters = railParameters;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor3D parentActor = actor as Actor3D;
            DrawnActor3D targetDrawnActor = this.TargetActor as DrawnActor3D;

            if (targetDrawnActor != null)
            {
                if (this.bFirstUpdate)
                {
                    //set the initial position of the camera
                    parentActor.Transform3D.Translation = railParameters.MidPoint;
                    this.bFirstUpdate = false;
                }

                //get look vector to target
                Vector3 cameraToTarget = MathUtility.GetNormalizedObjectToTargetVector(parentActor.Transform3D, targetDrawnActor.Transform3D);
                cameraToTarget = MathUtility.Round(cameraToTarget, 3);

                //new position for camera if it is positioned between start and end points of the rail
                Vector3 projectedCameraPosition = parentActor.Transform3D.Translation + Vector3.Dot(cameraToTarget, railParameters.Look)
                    * railParameters.Look; // * gameTime.ElapsedGameTime.Milliseconds;
                projectedCameraPosition = MathUtility.Round(projectedCameraPosition, 3); //round to prevent floating-point precision errors across updates
                //do not allow the camera to move outside the rail
                if (railParameters.InsideRail(projectedCameraPosition))
                {
                    parentActor.Transform3D.Translation = projectedCameraPosition;
                }

                //set the camera to look at the object
                parentActor.Transform3D.Look = cameraToTarget;
            }
        }
    }
}

