﻿/* Create a controller that moves the attached object along a user defined 3D curve*/

using GDLibrary.Curve;
using GDLibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Interfaces;
using Microsoft.Xna.Framework;
using GDLibrary.Actors;

namespace GDLibrary.Controllers.Base
{
    public class CurveController : GDLibrary.Controller.Base.Controller
    {
        private static readonly int DefaultCurveEvaluationPrecision = 4;
        #region Variables
        private Transform3DCurve transform3DCurve;
        private PlayStatusType playStatusType;
        private float elapsedTimeInMs;
        private int curveEvaluationPrecision;
        #endregion

        #region Properties
        public Transform3DCurve Transform3DCurve
        {
            get
            {
                return this.transform3DCurve;
            }

            set
            {
                this.transform3DCurve = value;
            }
        }

        public PlayStatusType PlayStatusType
        {
            get
            {
                return this.playStatusType;
            }

            set
            {
                this.playStatusType = value;
            }
        }

        public int CurveEvaluationPrecision
        {
            get
            {
                return this.curveEvaluationPrecision;
            }
            set
            {
                this.curveEvaluationPrecision = value;
            }
        }
        #endregion

        public CurveController(string id, ControllerType controllerType, Transform3DCurve transform3DCurve,
            PlayStatusType playStatusType)
            : this(id, controllerType, transform3DCurve, playStatusType, DefaultCurveEvaluationPrecision)
        {
        }

        public CurveController(string id, ControllerType controllerType,
            Transform3DCurve transform3DCurve, PlayStatusType playStatusType,
            int curveEvaluationPrecision)
            : base(id, controllerType)
        {
            this.transform3DCurve = transform3DCurve;
            this.playStatusType = playStatusType;
            this.elapsedTimeInMs = 0;
            this.curveEvaluationPrecision = curveEvaluationPrecision;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor3D parentActor = actor as Actor3D;
            if(this.playStatusType == PlayStatusType.Play)
            {
                UpdateTrack(gameTime, parentActor);
            }
            else if((this.playStatusType == PlayStatusType.Reset) || (this.playStatusType == PlayStatusType.Stop))
            {
                this.elapsedTimeInMs = 0;
            }
        }

        private void UpdateTrack(GameTime gameTime, Actor3D parentActor)
        {
            if(parentActor != null)
            {
                this.elapsedTimeInMs += gameTime.ElapsedGameTime.Milliseconds;

                Vector3 translation, look, up;
                this.transform3DCurve.Evaluate(elapsedTimeInMs, this.curveEvaluationPrecision, out translation, out look, out up);
                parentActor.Transform3D.Translation = translation;
                parentActor.Transform3D.Look = look;
                parentActor.Transform3D.Up = up;
            }
        }

    }
}
