﻿/* Creates a parent target controller which causes the parent actor (to which the controller is attached)
 * to follow a target e.g ThirdPersonCameraController or RailController*/
using GDLibrary.Enums;
using GDLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary.Controller.Base
{
    public class TargetController : GDLibrary.Controller.Base.Controller
    {
        #region Variables
        private IActor targetActor;
        #endregion

        #region Properties
        public IActor TargetActor
        {
            get
            {
                return this.targetActor;
            }

            set
            {
                this.targetActor = value;
            }
        }
        #endregion
        public TargetController(string id, ControllerType controllerType, IActor targetActor)
            : base(id, controllerType)
        {
            this.targetActor = targetActor;
        }
    }
}

