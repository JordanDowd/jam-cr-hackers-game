﻿#define DEMO

using GDLibrary;
using GDLibrary.Actors.Camera;
using GDLibrary.Actors.Drawn._2D.UI;
using GDLibrary.Actors.Drawn._3D;
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Actors.Drawn._3D.Collidable.Player.Animated;
using GDLibrary.Actors.Drawn._3D.Primitives;
using GDLibrary.Actors.Drawn2D.UI;
using GDLibrary.Controller._2D.Base;
using GDLibrary.Controller.Base;
using GDLibrary.Controller.Camera3D;
using GDLibrary.Controllers._2D.UI;
using GDLibrary.Controllers.Base;
using GDLibrary.Controllers.Camera3D;
using GDLibrary.Controllers.Camera3D.Base;
using GDLibrary.Controllers.Camera3D.Collidable;
using GDLibrary.Controllers.Camera3D.Object;
using GDLibrary.Controllers.GDVideo;
using GDLibrary.Curve;
using GDLibrary.Debug;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Factory;
using GDLibrary.GDDebug.Physics;
using GDLibrary.Interfaces;
using GDLibrary.Managers;
using GDLibrary.Managers.Camera;
using GDLibrary.Managers.Content;
using GDLibrary.Managers.Input;
using GDLibrary.Managers.Object;
using GDLibrary.Managers.Physics;
using GDLibrary.Managers.Picking;
using GDLibrary.Managers.Screen;
using GDLibrary.Managers.Sound;
using GDLibrary.Managers.UI;
using GDLibrary.Parameters.Camera;
using GDLibrary.Parameters.Effects;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Primitives;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Utility;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNAIntroClass.App.Actors;
using XNAIntroClass.Data;
using XNAIntroClass.Menu;

namespace XNAIntroClass
{
    public class Main : Microsoft.Xna.Framework.Game
    {
        #region Fields
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public ObjectManager objectManager { get; private set; }
        public CameraManager cameraManager { get; private set; }
        public MouseManager mouseManager { get; private set; }
        public KeyboardManager keyboardManager { get; private set; }
        public ScreenManager screenManager { get; private set; }
        public MyAppMenuManager menuManager { get; private set; }
        public PhysicsManager physicsManager { get; private set; }
        public UIManager uiManager { get; private set; }
        public UIManager uiManagerMouse { get; private set; }
        
        public GamePadManager gamePadManager { get; private set; }
        public SoundManager soundManager { get; private set; }
        public PickingManager pickingManager { get; private set; }
        public InventoryManager inventoryManager { get; private set; }
        private ManagerParameters managerParameters;

        //receives, handles and routes events
        public EventDispatcher eventDispatcher { get; private set; }

        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;
        private ContentDictionary<Video> videoDictionary;

        //stores curves and rails used by cameras
        private Dictionary<string, Transform3DCurve> curveDictionary;
        private Dictionary<string, RailParameters> railDictionary;
        //stores viewport layouts for multi-screen layout
        private Dictionary<string, Viewport> viewPortDictionary;
        private Dictionary<string, EffectParameters> effectDictionary;
        private Dictionary<string, IVertexData> vertexDataDictionary;


        //player
        private ModelObject pillarActivationDecoration1;
        private ModelObject pillarActivationDecoration2;
        private ModelObject pillarActivationDecoration3;
        private HeroPlayerObject heroPlayerObject;
        private TriangleMeshObject ringObject;
        private ModelObject drivableBoxObject;
        private AnimatedPlayerObject animatedHeroPlayerObject;
        private TriangleMeshObject Statue;
        private float upperScale = 1.2f;
        private float lowerScale = 1;
        private DebugDrawer debugDrawer;
        private PhysicsDebugDrawer physicsDebugDrawer;

        private double statueRadius = 20;
        private float ringSizeIncrease = 0.002f;
        private float ringSizeDecrease = 0.001f;
        private Vector3 ringMinimum = new Vector3(1, 1, 1);
        private Vector3 ringMaximum = new Vector3(2, 2, 2);
        private bool isOpaque;
        private bool isRingOpaque;
        private bool PillarEndPlayed;
        private float[] pillarAlphas;

        private bool demoStatues;

        #endregion

        #region Constructor
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            //moved instanciation here to allow menu and ui managers to be moved to InitializeManagers()
            spriteBatch = new SpriteBatch(GraphicsDevice);
            this.demoStatues = false;
            int gameLevel = 1;
            bool isMouseVisible = false;
            pillarAlphas = new float[3] { 0,0,0 };
            this.isOpaque = false;// for glowing effect on pillars
            this.isRingOpaque = false;
            this.PillarEndPlayed = false;
            Integer2 screenResolution = ScreenUtility.HD720;
            ScreenUtility.ScreenType screenType = ScreenUtility.ScreenType.SingleScreen;
            int numberOfGamePadPlayers = 1;

            
            //set the title
            Window.Title = "3DGD - My Amazing Game";

            //initialize the Dispatcher
            InitializeEventDispatcher();

            //Load dictionaries, media assets and non-media assets
            LoadDictionaries();
            LoadAssets();
            LoadCurvesAndRails();
            LoadViewports(screenResolution);

            //to draw primitives and billboards
            LoadVertexData();

            //Initilize Effects
            InitializeEffects();

            //Initialize the managers
            InitializeManagers(screenResolution, screenType, isMouseVisible, numberOfGamePadPlayers);

            //menu and UI elements
            AddMenuElements();
            AddUIElements();
#if DEBUG
            //InitializeDebugTextInfo();
#endif

            //load game happens before cameras are loaded as we may need a third person camera that needs a 
            //reference to a loaded actor
            LoadGame(gameLevel);

            //Initialize cameras based in the desired screen layout
            if(screenType == ScreenUtility.ScreenType.SingleScreen)
            {
                //InitializeCollidableFirstPersonDemo(screenResolution);
                //InitializeSingleScreenCycleableCameraDemo(screenResolution);
                InitializeCollidableThirdPersonDemo(screenResolution);
            }
            else
            {
                InitializeMultiScreenCameraDemo(screenResolution);
            }

            //Publish Game Start event
            StartGame();

#if DEBUG
           // InitializeDebugCollisionSkinInfo();
#endif

            base.Initialize();
        }

        private void InitializeManagers(Integer2 screenResolution, ScreenUtility.ScreenType screenType, bool isMouseVisible,
            int numberOfGamePadPlayers) //1-4
        {
            //add sound manager
            this.soundManager = new SoundManager(this, this.eventDispatcher, StatusType.Update, "Content/Assets/Audio/", 
                "Demo2DSound.xgs",  "WaveBank1.xwb", "SoundBank1.xsb");
            Components.Add(this.soundManager);

            this.inventoryManager = new InventoryManager(this, this.eventDispatcher);

            this.cameraManager = new CameraManager(this, 1, this.eventDispatcher);
            Components.Add(this.cameraManager);
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitPrimitivesEffectID].Clone() as BasicEffectParameters;
            this.objectManager = new ObjectManager(this, cameraManager, this.eventDispatcher, 10,this.modelDictionary, this.textureDictionary, this.inventoryManager);
            //Components.Add(this.objectManager);

            this.keyboardManager = new KeyboardManager(this);
            Components.Add(this.keyboardManager);

            //create the manager which supports multiple camera viewports
            this.screenManager = new ScreenManager(this, graphics, screenResolution,
                screenType, this.objectManager, this.cameraManager,
                this.keyboardManager, AppData.KeyPauseShowMenu, this.eventDispatcher, StatusType.Off);
            Components.Add(this.screenManager);

            this.physicsManager = new PhysicsManager(this, this.eventDispatcher, StatusType.Off, AppData.BigGravity);
            Components.Add(this.physicsManager);

            this.mouseManager = new MouseManager(this, isMouseVisible, this.physicsManager);
            Components.Add(this.mouseManager);

            // add gamepad manager
            if (numberOfGamePadPlayers > 0)
            {
                this.gamePadManager = new GamePadManager(this, numberOfGamePadPlayers);
                Components.Add(this.gamePadManager);
            }

            //menu manager
            this.menuManager = new MyAppMenuManager(this, this.mouseManager, this.keyboardManager, this.cameraManager, 
                spriteBatch, this.eventDispatcher, StatusType.Off);
            //set the main menu to be the active menu scene
            this.menuManager.SetActiveList("mainmenu");
            Components.Add(this.menuManager);

            //ui (e.g. reticule, inventory, progress)
            this.uiManager = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            Components.Add(this.uiManager);
            
            this.uiManagerMouse = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            Components.Add(this.uiManagerMouse);
            //this object packages together all managers to give the mouse object the ability to listen for all forms of input from the user, as well as know where camera is etc.
            this.managerParameters = new ManagerParameters(this.objectManager,
                this.cameraManager, this.mouseManager, this.keyboardManager, this.gamePadManager, this.screenManager, this.soundManager);

            #region Pick Manager
            //call this function anytime we want to decide if a mouse over object is interesting to the PickingManager
            //See https://www.codeproject.com/Articles/114931/Understanding-Predicate-Delegates-in-C
            Predicate<CollidableObject> collisionPredicate = new Predicate<CollidableObject>(CollisionUtility.IsCollidableObjectOfInterest);
            //create the projectile archetype that the manager can fire

            //listens for picking with the mouse on valid (based on specified predicate) collidable objects and pushes notification events to listeners
            this.pickingManager = new PickingManager(this, this.eventDispatcher, StatusType.Off,
                this.managerParameters,
                PickingBehaviourType.PickAndRemove, AppData.PickStartDistance, AppData.PickEndDistance, collisionPredicate);
            Components.Add(this.pickingManager);
            #endregion

           

        }

        private void InitializeUIInventoryMenu()
        {
            this.eventDispatcher.InventoryEvent += UpdateInventoryList;

            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null;
            int verticalBtnSeparation = 70;

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitPrimitivesEffectID].Clone() as BasicEffectParameters;


            string sceneID = "", buttonID = "", buttonText = "";


            Vector2 scale;
            sceneID = "inventory list";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            buttonID = "backbtn";
            buttonText = "Back";
            position = new Vector2(graphics.PreferredBackBufferWidth / 4.8f, 200);
            texture = this.textureDictionary["genericbtn"];
            transform = new Transform2D(position,
                0, new Vector2((1.5f)/2.5f, (0.6f)/3f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.Black, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);
        }

        private void UpdateInventoryList(EventData eventData)
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 70;

            Vector2 scale;
            sceneID = "inventory list";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));



            int i = 2;

            foreach (string item in inventoryManager.Inventory)
            {
                buttonID = "itembtn_" + item;
                buttonText = item;
                position = new Vector2(graphics.PreferredBackBufferWidth / 4.8f, 200 + i * verticalBtnSeparation);
                texture = this.textureDictionary["genericbtn"];
                transform = new Transform2D(position,
                    0, new Vector2((1.5f) / 3f, (0.6f) / 2.5f),
                    new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

                uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                    transform, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                    this.fontDictionary["menu"],
                    Color.Black, new Vector2(0, 2));

                // uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
                // new TrigonometricParameters(0.1f, 0.2f, 1)));
                this.menuManager.Add(sceneID, uiButtonObject);
                i++;
            }


        }


        private void LoadDictionaries()
        {
            //models
            this.modelDictionary = new ContentDictionary<Model>("model dictionary", this.Content);

            //textures
            this.textureDictionary = new ContentDictionary<Texture2D>("texture dictionary", this.Content);

            //fonts
            this.fontDictionary = new ContentDictionary<SpriteFont>("font dictionary", this.Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            this.curveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails
            this.railDictionary = new Dictionary<string, RailParameters>();

            //viewports - used to store different viewports to be applied to multi-screen layouts
            this.viewPortDictionary = new Dictionary<string, Viewport>();

            //stores default effect parameters
            this.effectDictionary = new Dictionary<string, EffectParameters>();

            //notice we go back to using a content dictionary type since we want to pass strings and have dictionary load content
            this.videoDictionary = new ContentDictionary<Video>("video dictionary", this.Content);

            //used to store IVertexData (i.e. when we want to draw primitive objects, as in I-CA)
            this.vertexDataDictionary = new Dictionary<string, IVertexData>();


        }

        private void LoadAssets()
        {
            #region Models
            this.modelDictionary.Load("Assets/Models/plane1", "plane1");
            this.modelDictionary.Load("Assets/Models/box", "box");
            this.modelDictionary.Load("Assets/Models/box2", "box2");
            //this.modelDictionary.Load("Assets/Models/torus", "torus");
            this.modelDictionary.Load("Assets/Models/sphere", "sphere");

            ////triangle mesh for the high/medium and low poly versions
            //this.modelDictionary.Load("Assets/Models/teapot");
            //this.modelDictionary.Load("Assets/Models/teapot_mediumpoly");
            //this.modelDictionary.Load("Assets/Models/teapot_lowpoly");

            ////player - replace with animation eventually
            //this.modelDictionary.Load("Assets/Models/cylinder");

            ////architecture
            //this.modelDictionary.Load("Assets/Models/Architecture/Buildings/house");
            //this.modelDictionary.Load("Assets/Models/Architecture/Walls/wall");

            ////dual texture demo
            //this.modelDictionary.Load("Assets/Models/box");
            this.modelDictionary.Load("Assets/Models/box1");

            this.modelDictionary.Load("Assets/Models/MyModels/trees", "trees");
            this.modelDictionary.Load("Assets/Models/MyModels/pillar01", "pillar_1");
            this.modelDictionary.Load("Assets/Models/MyModels/pillar_large", "pillar_2");
            this.modelDictionary.Load("Assets/Models/MyModels/pillar_collection", "pillar_3");
            this.modelDictionary.Load("Assets/Models/MyModels/pillar_interaction", "pillar_i");
            this.modelDictionary.Load("Assets/Models/MyModels/player", "player");
            this.modelDictionary.Load("Assets/Models/MyModels/ring_new", "ring");
            this.modelDictionary.Load("Assets/Models/MyModels/scene_complete_2", "SC02");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff", "cliff_1");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_2", "cliff_2");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_3", "cliff_3");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_4", "cliff_4");
            this.modelDictionary.Load("Assets/Models/MyModels/poll", "poll");
            this.modelDictionary.Load("Assets/Models/MyModels/trees_pillar_top", "trees_pt");
            this.modelDictionary.Load("Assets/Models/MyModels/trees_bottom_and_right", "trees_bar");
            this.modelDictionary.Load("Assets/Models/MyModels/ground", "ground");
            this.modelDictionary.Load("Assets/Models/MyModels/ground_base", "ground_base");
            this.modelDictionary.Load("Assets/Models/MyModels/Sword");
            this.modelDictionary.Load("Assets/Models/MyModels/pillar_actived", "P_A");
            this.modelDictionary.Load("Assets/Models/MyModels/Statue");
            

            //LowPoly
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_low_poly");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_2_low_poly");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_3_low_poly");
            this.modelDictionary.Load("Assets/Models/MyModels/cliff_4_low_poly");
            this.modelDictionary.Load("Assets/Models/MyModels/trees_bar_low_poly");
            this.modelDictionary.Load("Assets/Models/MyModels/StatueLowPoly");

            #endregion

            #region Textures
            //environment
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate1"); //demo use of the shorter form of Load() that generates key from asset name
            //this.textureDictionary.Load("Assets/Debug/Textures/checkerboard");
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate2");
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/grass1");
            this.textureDictionary.Load("Assets/Textures/Skybox/back");
            this.textureDictionary.Load("Assets/Textures/Skybox/left");
            this.textureDictionary.Load("Assets/Textures/Skybox/right");
            this.textureDictionary.Load("Assets/Textures/Skybox/sky");
            this.textureDictionary.Load("Assets/Textures/Skybox/front");
            this.textureDictionary.Load("Assets/Textures/Foliage/Trees/tree2");
            this.textureDictionary.Load("Assets/Textures/checkerboard");
            this.textureDictionary.Load("Assets/Textures/checkerboard_greywhite");

            //menu - buttons
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/genericbtn");

            //menu - backgrounds
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/mainmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/audiomenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/controlsmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/exitmenuwithtrans");

            this.textureDictionary.Load("Assets/Textures/MyTextures/trees");
            this.textureDictionary.Load("Assets/Textures/MyTextures/render");
            this.textureDictionary.Load("Assets/Textures/MyTextures/pillar_large_texture");
            this.textureDictionary.Load("Assets/Textures/MyTextures/pillar_set_texture");
            this.textureDictionary.Load("Assets/Textures/MyTextures/scene");
            this.textureDictionary.Load("Assets/Textures/MyTextures/grass");
            this.textureDictionary.Load("Assets/Textures/MyTextures/ground_base");
            this.textureDictionary.Load("Assets/Textures/MyTextures/player");
            this.textureDictionary.Load("Assets/Textures/MyTextures/ring_texture");
            this.textureDictionary.Load("Assets/Textures/MyTextures/pillar_interaction_texture");
            this.textureDictionary.Load("Assets/Textures/MyTextures/cliff");
            this.textureDictionary.Load("Assets/Textures/MyTextures/mountain");
            this.textureDictionary.Load("Assets/Textures/MyTextures/Sword");
            this.textureDictionary.Load("Assets/Textures/MyTextures/pillar_active");
            this.textureDictionary.Load("Assets/Textures/Skybox/sky_2");
            this.textureDictionary.Load("Assets/Textures/MyTextures/Statue");
            this.textureDictionary.Load("Assets/Textures/MyTextures/StatueGlow");
            #endregion

            //ui (or hud) elements
            this.textureDictionary.Load("Assets/Textures/UI/HUD/progress_white");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/reticuleDefault");
           
            //architecture


            //dual texture demo - see Main::InitializeCollidableGround()
           // this.textureDictionary.Load("Assets/Debug/Textures/checkerboard_greywhite");

            #region Fonts
            this.fontDictionary.Load("Assets/Debug/Fonts/debug");
            this.fontDictionary.Load("Assets/Fonts/menu");
            this.fontDictionary.Load("Assets/Fonts/mouse");
            #endregion

            #region Video
            this.videoDictionary.Load("Assets/Video/sample");
            this.videoDictionary.Load("Assets/Video/Wildlife");
            #endregion

            #region Animations
            //contains a single animation "Take001"
            this.modelDictionary.Load("Assets/Models/Animated/dude");

            //squirrel - one file per animation
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Idle");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Jump");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Punch");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Standing");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Tailwhip");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/RedRun4");
            #endregion
        }

        private void LoadCurvesAndRails()
        {
            int cameraHeight = 5;

            #region Curves
            //create the camera curve to be applied to the track controller
            Transform3DCurve curveA = new Transform3DCurve(CurveLoopType.Oscillate); //experiment with other CurveLoopTypes
            curveA.Add(new Vector3(40, cameraHeight, 80), -Vector3.UnitX, Vector3.UnitY, 0); //start position
            curveA.Add(new Vector3(0, 10, 60), -Vector3.UnitZ, Vector3.UnitY, 4);
            curveA.Add(new Vector3(0, 40, 0), -Vector3.UnitY, -Vector3.UnitZ, 8); //curve mid-point
            curveA.Add(new Vector3(0, 10, 60), -Vector3.UnitZ, Vector3.UnitY, 12);
            curveA.Add(new Vector3(40, cameraHeight, 80), -Vector3.UnitX, Vector3.UnitY, 16); //end position - same as start for zero-discontinuity on cycle
            //add to the dictionary
            this.curveDictionary.Add("unique curve name 1", curveA);
            #endregion

            #region Rails
            //create the track to be applied to the non-collidable track camera 1
            this.railDictionary.Add("rail1 - parallel to x-axis", new RailParameters("rail1 - parallel to x-axis", new Vector3(-80, 10, 40), new Vector3(80, 10, 40)));
            #endregion
        }

        private void LoadVertexData()
        {
            Microsoft.Xna.Framework.Graphics.PrimitiveType primitiveType;
            int primitiveCount;
            IVertexData vertexData = null;

            #region Textured Quad
            //get vertices for textured quad
            VertexPositionColorTexture[] vertices = VertexFactory.GetTextureQuadVertices(out primitiveType, out primitiveCount);

            //make a vertex data object to store and draw the vertices
            vertexData = new BufferedVertexData<VertexPositionColorTexture>(this.graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            //add to the dictionary for use by things like billboards - see InitializeBillboards()
            this.vertexDataDictionary.Add(AppData.TexturedQuadID, vertexData);
            #endregion

            //#region Billboard Quad - we must use this type when creating billboards
            //// get vertices for textured billboard
            //VertexBillboard[] verticesBillboard = VertexFactory.GetVertexBillboard(1, out primitiveType, out primitiveCount);

            ////make a vertex data object to store and draw the vertices
            //vertexData = new BufferedVertexData<VertexBillboard>(this.graphics.GraphicsDevice, verticesBillboard, primitiveType, primitiveCount);

            ////add to the dictionary for use by things like billboards - see InitializeBillboards()
            //this.vertexDataDictionary.Add(AppData.TexturedBillboardQuadID, vertexData);
            //#endregion

        }

        private void LoadViewports(Integer2 screenResolution)
        {
           
            //the full screen viewport with optional padding
            int leftPadding = 0, topPadding = 0, rightPadding = 0, bottomPadding = 0;
            Viewport paddedFullViewPort = ScreenUtility.Pad(new Viewport(0, 0, screenResolution.X, (int)(screenResolution.Y)), leftPadding, topPadding, rightPadding, bottomPadding);
            this.viewPortDictionary.Add("full viewport", paddedFullViewPort);

            //work out the dimensions of the small camera views along the left hand side of the screen
            int smallViewPortHeight = 144; //6 small cameras along the left hand side of the main camera view i.e. total height / 5 = 720 / 5 = 144
            int smallViewPortWidth = 5 * smallViewPortHeight / 3; //we should try to maintain same ProjectionParameters aspect ratio for small cameras as the large     
            //the five side viewports in multi-screen mode
            this.viewPortDictionary.Add("column0 row0", new Viewport(0, 0, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row1", new Viewport(0, 1 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row2", new Viewport(0, 2 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row3", new Viewport(0, 3 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row4", new Viewport(0, 4 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            //the larger view to the right in column 1
            this.viewPortDictionary.Add("column1 row0", new Viewport(smallViewPortWidth, 0, screenResolution.X - smallViewPortWidth, screenResolution.Y));
        }

        #if DEBUG
        private void InitializeDebugTextInfo()
        {
            //add debug info in top left hand corner of the screen
            this.debugDrawer = new DebugDrawer(this, this.screenManager, this.cameraManager, this.objectManager, this.inventoryManager, spriteBatch,
                this.fontDictionary["debug"], Color.White, new Vector2(5, 5), this.eventDispatcher, StatusType.Off);
            Components.Add(this.debugDrawer);

        }

        private void InitializeDebugCollisionSkinInfo()
        {
            //show the collision skins
            this.physicsDebugDrawer = new PhysicsDebugDrawer(this, this.cameraManager, this.objectManager,
                this.screenManager, this.eventDispatcher, StatusType.Off);
            Components.Add(this.physicsDebugDrawer);
        }
        #endif
        #endregion

        #region Load Game Content
        //load the contents for the level specified
        private void LoadGame(int level)
        {
            int worldScale = 250;

            //Non-collidable
            InitializeNonCollidableSkyBox(worldScale);
            //InitializeNonCollidableFoliage(worldScale);
            ///InitializeNonCollidableDriveableObject();
            InitializeNonCollidableDecoratorObjects();

            //Collidable
            InitializeCollidableGround(worldScale);
            
            //demo medium and low vertex count trianglemesh
            InitializeStaticCollidableMediumPolyTriangleMeshObjects();
            InitializeStaticCollidableLowPolyTriangleMeshObjects();
            //demo dynamic collidable objects with user-defined collision primitives
            InitializeDynamicCollidableObjects();
            InitializeNonCollidableDriveableObject();
            //demo high vertex count trianglemesh
            InitializeStaticCollidableTriangleMeshObjects();
            //adds the hero of the game - see InitializeSingleScreenFirstThirdPersonDemo()
            //InitializeCollidableHeroPlayerObject();
            ////add level elements
            InitializeBuildings();
            InitializeWallsFences();

            //add video display
            InitializeVideoDisplay();

            //add animated characters
            bool bTheDudeAbides = true;
            if (bTheDudeAbides)
                InitializeDudeAnimatedPlayer();
            else
                InitializeSquirrelAnimatedPlayer();

            ////add primitive objects - where developer defines the vertices manually
            InitializePrimitives();
            InitializeRing();
        }

        private void InitializeRing()
        {
            Transform3D transform3D = null;

            transform3D = new Transform3D(new Vector3(0, 20, 40),
                new Vector3(0, 0, 0),
                 Vector3.One,
                 -Vector3.UnitZ, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //remember we can set diffuse color and alpha too but not specular, emissive, directional lights as I dont read those parameters in ObjectManager::DrawObject() - this was purely a time constraint on my part.

            effectParameters.Texture = this.textureDictionary["ring_texture"];
            effectParameters.SpecularColor = Color.White;
            effectParameters.SpecularPower = 1;
            //effectParameters.Alpha = 1;
            //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
            //effectParameters.Texture = this.textureDictionary["checkerboard"];
            effectParameters.Alpha = 0.7f;
            ringObject = new TriangleMeshObject("ring", ActorType.CollidableProp, transform3D, effectParameters,
                            this.modelDictionary["ring"], new MaterialProperties(0.2f, 0.8f, 0.7f));

            ringObject.AttachController(new RingController("Ring", ControllerType.Ring, this.animatedHeroPlayerObject));
            ringObject.Enable(false, 1);
            this.objectManager.Add(ringObject);

        }
        private void InitializePrimitives()
        {
            //get a copy of the effect parameters
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitPrimitivesEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            effectParameters.DiffuseColor = Color.Yellow;
            effectParameters.Alpha = 0.4f;

            //define location
            Transform3D transform = new Transform3D(new Vector3(0, 40, 0), new Vector3(40, 4, 1));

            //create primitive
            PrimitiveObject primitiveObject = new PrimitiveObject("simple primitive", ActorType.Primitive,
                transform, effectParameters, StatusType.Drawn | StatusType.Update, this.vertexDataDictionary[AppData.TexturedQuadID]);

            PrimitiveObject clonedPrimitiveObject = null;

            for (int i = 1; i <= 4; i++)
            {
                clonedPrimitiveObject = primitiveObject.Clone() as PrimitiveObject;
                clonedPrimitiveObject.Transform3D.Translation += new Vector3(0, 5 * i, 0);

                //we could also attach controllers here instead to give each a different rotation
                clonedPrimitiveObject.AttachController(new RotationController("rot controller", ControllerType.Rotation, new Vector3(0.1f * i, 0, 0)));

                //add to manager
                this.objectManager.Add(clonedPrimitiveObject);
            }

        }

        private void InitializeSquirrelAnimatedPlayer()
        {
            Transform3D transform3D = null;

            transform3D = new Transform3D(new Vector3(0, 20, 30),
                new Vector3(-90, 0, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 2f * Vector3.One,
                 Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.DiffuseColor = Color.OrangeRed;
            //no specular, emissive, directional lights
            //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
            effectParameters.Texture = this.textureDictionary["checkerboard_greywhite"];

            this.animatedHeroPlayerObject = new SquirrelAnimatedPlayerObject("squirrel",
            ActorType.Player, transform3D,
                effectParameters,
                AppData.PlayerOneMoveKeys,
                AppData.PlayerRadius,
                AppData.PlayerHeight,
                1, 1,  //accel, decel
                AppData.SquirrelPlayerMoveSpeed,
                AppData.SquirrelPlayerRotationSpeed,
                AppData.PlayerJumpHeight,
                new Vector3(0, 0, 0), //offset inside capsule
                this.keyboardManager);
            this.animatedHeroPlayerObject.Enable(false, AppData.PlayerMass);

            //add the animations
            string takeName = "Take 001";
            string fileNameNoSuffix = "Red_Idle";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Jump";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Punch";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Standing";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Tailwhip";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "RedRun4";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);

            //set the start animtion
            this.animatedHeroPlayerObject.SetAnimation("Take 001", "Red_Idle");

            this.objectManager.Add(animatedHeroPlayerObject);

        }

        private void InitializeDudeAnimatedPlayer()
        {
            Transform3D transform3D = null;

            transform3D = new Transform3D(new Vector3(-15, 15, 66),
                new Vector3(-90, 0, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 0.1f * Vector3.One,
                 -Vector3.UnitZ, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //remember we can set diffuse color and alpha too but not specular, emissive, directional lights as I dont read those parameters in ObjectManager::DrawObject() - this was purely a time constraint on my part.
            effectParameters.DiffuseColor = Color.White;
            effectParameters.Alpha = 1;
            //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
            effectParameters.Texture = null;

            this.animatedHeroPlayerObject = new DudeAnimatedPlayerObject("dude",
                    ActorType.Player, transform3D,
                effectParameters,
                AppData.SquirrelPlayerMoveKeys,
                AppData.PlayerRadius,
                AppData.PlayerHeight,
                1, 1,  //accel, decel
                AppData.DudeMoveSpeed,
                AppData.DudeRotationSpeed,
                AppData.DudeJumpHeight,
                new Vector3(0, -3.5f, 0), //offset inside capsule - purely cosmetic
                this.keyboardManager);
            this.animatedHeroPlayerObject.Enable(false, AppData.PlayerMass);

            string takeName = "Take 001";
            string fileNameNoSuffix = "dude";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);

            //set the start animtion
            this.animatedHeroPlayerObject.SetAnimation("Take 001", "dude"); //basically take name (default from 3DS Max) and FBX file name with no suffix

            this.objectManager.Add(animatedHeroPlayerObject);

        }

        private void InitializeCollidableHeroPlayerObject()
        {
            Transform3D transform = new Transform3D(new Vector3(0, 25, 20),
                new Vector3(90, 0, 0),
                new Vector3(1, 3.5f, 1), -Vector3.UnitZ, Vector3.UnitY);

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            //make the hero a field since we need to point the third person camera controller at this object
            this.heroPlayerObject = new HeroPlayerObject(AppData.PlayerOneID,
                AppData.PlayerOneProgressControllerID, //used to increment/decrement progress on pickup, win, or lose
                ActorType.Player, transform, effectParameters,
                this.modelDictionary["cylinder"],
                AppData.PlayerTwoMoveKeys,
                AppData.PlayerRadius, AppData.PlayerHeight,
                1.8f, 1.7f,
                AppData.PlayerJumpHeight,
                new Vector3(0, 5, 0),
                this.keyboardManager);
            this.heroPlayerObject.Enable(false, AppData.PlayerMass);

            //don't forget to add it - or else we wont see it!
            this.objectManager.Add(this.heroPlayerObject);
        }

        //skybox is a non-collidable series of ModelObjects with no lighting
        private void InitializeNonCollidableSkyBox(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the skybox decorator elements (e.g. ground, front, top etc). 
            Transform3D transform = new Transform3D(new Vector3(0, -5, 0), new Vector3(worldScale, 1, worldScale));

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, 
                effectParameters,
                this.modelDictionary["plane1"]);

            //will be re-used for all planes
            ModelObject clonePlane = null;

            #region Skybox
            //we no longer add a simple grass plane - see InitializeCollidableGround()
            //clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            //clonePlane.Texture = this.textureDictionary["grass1"];
            //this.objectManager.Add(clonePlane);

            //add the back skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["back"];
            //rotate the default plane 90 degrees around the X-axis (use the thumb and curled fingers of your right hand to determine +ve or -ve rotation value)
            clonePlane.Transform3D.Rotation = new Vector3(90, 0, 0);
            /*
             * Move the plane back to meet with the back edge of the grass (by based on the original 3DS Max model scale)
             * Note:
             * - the interaction between 3DS Max and XNA units which result in the scale factor used below (i.e. 1 x 2.54 x worldScale)/2
             * - that I move the plane down a little on the Y-axiz, purely for aesthetic purposes
             */
            clonePlane.Transform3D.Translation = new Vector3(0, -5, (-2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);

            //As an exercise the student should add the remaining 4 skybox planes here by repeating the clone, texture assignment, rotation, and translation steps above...
            //add the left skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["left"];
            clonePlane.Transform3D.Rotation = new Vector3(90, 90, 0);
            clonePlane.Transform3D.Translation = new Vector3((-2.54f * worldScale) / 2.0f, -5, 0);
            this.objectManager.Add(clonePlane);

            //add the right skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["right"];
            clonePlane.Transform3D.Rotation = new Vector3(90, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3((2.54f * worldScale) / 2.0f, -5, 0);
            this.objectManager.Add(clonePlane);

            //add the top skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["sky"];
            //notice the combination of rotations to correctly align the sky texture with the sides
            clonePlane.Transform3D.Rotation = new Vector3(180, -90, 0);
            clonePlane.Transform3D.Translation = new Vector3(0, ((2.54f * worldScale) / 2.0f) - 5, 0);
            this.objectManager.Add(clonePlane);

            //add the front skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["front"];
            clonePlane.Transform3D.Rotation = new Vector3(-90, 0, 180);
            clonePlane.Transform3D.Translation = new Vector3(0, -5, (2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);
            #endregion
        }

        private void InitializeNonCollidableFoliage(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the decorator elements (e.g. trees etc). 
            Transform3D transform = new Transform3D(new Vector3(0, -5, 0), new Vector3(worldScale, 1, worldScale));

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            //a fix to ensure that any image containing transparent pixels will be sent to the correct draw list in ObjectManager
            effectParameters.Alpha = 0.99f;

            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, effectParameters,
                this.modelDictionary["plane1"]);
        }

        private void InitializeCollidableGround(int worldScale)
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;

            Model model = this.modelDictionary["ground"];

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;

            effectParameters.Texture = this.textureDictionary["grass"];

            transform3D = new Transform3D(new Vector3(14, 0, -23), new Vector3(0, 0, 0),
                Vector3.One, Vector3.UnitX, Vector3.UnitY);
            collidableObject = new CollidableObject("ground", ActorType.CollidableGround, transform3D, effectParameters, model);
            collidableObject.AddPrimitive(new JigLibX.Geometry.Plane(transform3D.Up, transform3D.Translation), new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false, see what happens.
            this.objectManager.Add(collidableObject);

        }

        //Triangle mesh objects wrap a tight collision surface around complex shapes - the downside is that TriangleMeshObjects CANNOT be moved
        private void InitializeStaticCollidableTriangleMeshObjects()
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            BasicEffectParameters effectParametersPillar = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            BasicEffectParameters effectParametersPillarMedium = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            BasicEffectParameters effectParametersPillarLarge = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            BasicEffectParameters effectParametersPillarSet = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;

            #region Torus - Uncommented
            /*
            transform3D = new Transform3D(new Vector3(-30, 0, 0),
                new Vector3(45, 45, 0), 0.1f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("torus", ActorType.CollidableProp,
            transform3D, this.modelEffect,
            ColorParameters.WhiteOpaque,
            this.textureDictionary["checkerboard"], this.modelDictionary["torus"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
            */
            #endregion

            #region Pillars - Interaction
            effectParametersPillar.Texture = this.textureDictionary["pillar_interaction_texture"];
            transform3D = new Transform3D(new Vector3(-3f, 0, -100),
                new Vector3(0, -160, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Interaction01", ActorType.CollidableProp,
            transform3D, effectParametersPillar, this.modelDictionary["pillar_i"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(3.1f, 0, -126.1f),
                new Vector3(0, -160, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Interaction02", ActorType.CollidableProp,
            transform3D, effectParametersPillar, this.modelDictionary["pillar_i"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-25.1f, 0, -116.1f),
                new Vector3(0, -160, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Interaction03", ActorType.CollidableProp,
            transform3D, effectParametersPillar, this.modelDictionary["pillar_i"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
            #endregion

            #region Pillars - Medium
            effectParametersPillarMedium.Texture = this.textureDictionary["render"];
            transform3D = new Transform3D(new Vector3(38.3f, 0, -107.9f),
                new Vector3(0, -288, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
   
            collidableObject = new TriangleMeshObject("pillar_Medium01", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);


            transform3D = new Transform3D(new Vector3(26.3f, 0, -108.7f),
                new Vector3(0, -478.6f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium02", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(17.9f, 0, -126.3f),
               new Vector3(0, -210, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium03", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(9.6f, 0, -134.6f),
               new Vector3(0, -289.4f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium04", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-36.2f, 0, -117.7f),
               new Vector3(0, -480.7f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium05", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-37.8f, 0, -105.9f),
               new Vector3(0, -479.8f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium06", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-36.1f, 0, -86.9f),
               new Vector3(0, 429.5f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium07", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-45.1f, 0, -79.9f),
               new Vector3(0, 159.8f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Medium08", ActorType.CollidableProp,
            transform3D, effectParametersPillarMedium, this.modelDictionary["pillar_1"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
            #endregion

            #region Pillars - Large
            effectParametersPillarLarge.Texture = this.textureDictionary["pillar_large_texture"];

            transform3D = new Transform3D(new Vector3(32.2f, 0, -108),
                new Vector3(0, -249, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Large01", ActorType.CollidableProp,
            transform3D, effectParametersPillarLarge, this.modelDictionary["pillar_2"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(14.4f, 0, -131.3f),
                new Vector3(0, -406.8f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Large02", ActorType.CollidableProp,
            transform3D, effectParametersPillarLarge, this.modelDictionary["pillar_2"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-37.7f, 0, -112.3f),
                new Vector3(0, -540.2f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Large03", ActorType.CollidableProp,
            transform3D, effectParametersPillarLarge, this.modelDictionary["pillar_2"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-39.4f, 0, -82.7f),
                new Vector3(0, 290, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Large03", ActorType.CollidableProp,
            transform3D, effectParametersPillarLarge, this.modelDictionary["pillar_2"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
            #endregion

            #region Pillar Set
            effectParametersPillarSet.Texture = this.textureDictionary["pillar_set_texture"];

            transform3D = new Transform3D(new Vector3(-1.1f, 0, -137.5f),
                new Vector3(0, 90f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Set01", ActorType.CollidableProp,
            transform3D, effectParametersPillarSet, this.modelDictionary["pillar_3"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-16.1f, 0, -136f),
                new Vector3(0, -70, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Set02", ActorType.CollidableProp,
            transform3D, effectParametersPillarSet, this.modelDictionary["pillar_3"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-29.3f, 0, -127.3f),
                new Vector3(0, 135.1f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Set04", ActorType.CollidableProp,
            transform3D, effectParametersPillarSet, this.modelDictionary["pillar_3"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-35.6f, 0, -96.1f),
                new Vector3(0, 200.4f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Set05", ActorType.CollidableProp,
            transform3D, effectParametersPillarSet, this.modelDictionary["pillar_3"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(22, 0, -117.3f),
                new Vector3(0, 382f, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("pillar_Set06", ActorType.CollidableProp,
            transform3D, effectParametersPillarSet, this.modelDictionary["pillar_3"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            #endregion
        }

        //Demo use of a low-poly model to generate the triangle meshh collision skin
        //This is much more efficient than using a full mesh on the model
        private void InitializeStaticCollidableMediumPolyTriangleMeshObjects()
        {
            #region Teapot - uncommented
            //Transform3D transform3D = new Transform3D(new Vector3(-30, 3, 0),
            //    new Vector3(0, 0, 0), 0.08f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            //BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //effectParameters.Texture = this.textureDictionary["checkerboard"];

            //CollidableObject collidableObject = new TriangleMeshObject("teapot", ActorType.CollidableProp, transform3D, effectParameters,
            //            this.modelDictionary["teapot"], this.modelDictionary["teapot_mediumpoly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            //collidableObject.Enable(true, 1);
            //this.objectManager.Add(collidableObject);
            #endregion
        }

        //Demos use of a low-polygon model to generate the triangle mesh collision skin - saving CPU cycles on CDCR checking
        private void InitializeStaticCollidableLowPolyTriangleMeshObjects()
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            BasicEffectParameters effectParametersCliff = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            BasicEffectParameters effectParametersTrees = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;

            effectParametersCliff.SpecularPower = 200;
            #region Cliffs
            effectParametersCliff.Texture = this.textureDictionary["cliff"];
            transform3D = new Transform3D(new Vector3(63, 0, -83), new Vector3(0, 0, 0), Vector3.One,
                Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("cliff", ActorType.CollidableProp, transform3D, effectParametersCliff, this.modelDictionary["cliff_1"],
                this.modelDictionary["cliff_low_poly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-52, 0, -29), new Vector3(0, 0, 0), Vector3.One,
                Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("cliff_2", ActorType.CollidableProp, transform3D, effectParametersCliff, this.modelDictionary["cliff_2"],
                this.modelDictionary["cliff_2_low_poly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(-16, 0, -137), new Vector3(0, 0, 0), Vector3.One,
                Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("cliff_3", ActorType.CollidableProp, transform3D, effectParametersCliff, this.modelDictionary["cliff_3"],
                this.modelDictionary["cliff_3_low_poly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(15.6f, 0, 70.6f), new Vector3(0, 0, 0), Vector3.One,
                Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("cliff_4", ActorType.CollidableProp, transform3D, effectParametersCliff, this.modelDictionary["cliff_4"],
                this.modelDictionary["cliff_4_low_poly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
            #endregion

            #region Trees
            effectParametersTrees.Texture = this.textureDictionary["trees"];
            transform3D = new Transform3D(new Vector3(-16, 0, -137), new Vector3(0, 0, 0), Vector3.One,
                Vector3.UnitX, Vector3.UnitY);

            collidableObject = new TriangleMeshObject("trees_pillar_area", ActorType.CollidableProp, transform3D, effectParametersTrees, this.modelDictionary["trees_pt"],
                this.modelDictionary["cliff_3_low_poly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);

            transform3D = new Transform3D(new Vector3(26, 0, 36), new Vector3(0, 0, 0), Vector3.One,
                Vector3.UnitX, Vector3.UnitY);
            collidableObject = new TriangleMeshObject("trees_bottom_and_right", ActorType.CollidableProp, transform3D, effectParametersTrees, this.modelDictionary["trees_bar"],
                this.modelDictionary["trees_bar_low_poly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
            #endregion
        }

        //if you want objects to be collidable AND moveable then you must attach either a box, sphere, or capsule primitives to the object
        private void InitializeDynamicCollidableObjects()
        {

            BasicEffectParameters effectParametersSword = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            BasicEffectParameters effectParametersStatue = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            
            #region Sword
            effectParametersSword.Texture = this.textureDictionary["Sword"];

            Transform3D transform3D = new Transform3D(new Vector3(47f, 2.3f, 20.4f),
            new Vector3(-90, 90, 0), new Vector3(0.1f, 0.1f, 0.1f), Vector3.UnitX, Vector3.UnitY);

            TriangleMeshObject Sword = new TriangleMeshObject("Magical Sword", ActorType.CollidablePickup,
            transform3D, effectParametersSword, this.modelDictionary["Sword"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
            Sword.Enable(true, 1);
            this.objectManager.Add(Sword);
            #endregion

            if (this.demoStatues)
            {
                DrawDemoStatues();
            }
            else
            {

                effectParametersStatue.Texture = this.textureDictionary["Statue"];
                transform3D = new Transform3D(new Vector3(90, 0, 10), new Vector3(0, -65, 0), new Vector3(0.008f, 0.008f, 0.008f),
                Vector3.UnitX, Vector3.UnitY);

                this.Statue = new TriangleMeshObject("Statue", ActorType.InteractiveProp,
                transform3D, effectParametersStatue, this.modelDictionary["Statue"], this.modelDictionary["StatueLowPoly"],
                new MaterialProperties(0.2f, 0.8f, 0.7f));
                Statue.Enable(true, 1);
                this.objectManager.Add(Statue);
            }
            #endregion


            //if (this.inventoryManager.InventoryContains("Sword"))
            //{
            //    this.drivableRingObject.Transform3D.Scale += new Vector3(1f, 0, 1f);

            //    Transform3D transform3D = new Transform3D(new Vector3(86.1f, 18.8f, 10.3f),
            //    new Vector3(90, -90, 0), new Vector3(0.1f, 0.1f, 0.1f), Vector3.UnitX, Vector3.UnitY);


            //    CollidableObject collidableObject = new TriangleMeshObject("Sword", ActorType.CollidableProp,
            //    transform3D, this.modelEffect,
            //    ColorParameters.WhiteOpaque,
            //    this.textureDictionary["Sword"], this.modelDictionary["Sword"],
            //        new MaterialProperties(0.2f, 0.8f, 0.7f));
            //    collidableObject.Enable(true, 1);

            //    this.objectManager.Add(collidableObject);
            //}

        }

        //demo of non-collidable ModelObject with attached third person controller
        private void InitializeNonCollidableDriveableObject()
        {
            ////place the drivable model to the left of the existing models and specify that forward movement is along the -ve z-axis
            //Transform3D transform = new Transform3D(new Vector3(-10, 3, 25), -Vector3.UnitZ, Vector3.UnitY);

            //BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //effectParameters.Texture = this.textureDictionary["crate1"];
            //effectParameters.DiffuseColor = Color.Gold;

            ////initialise the drivable model object - we've made this variable a field to allow it to be visible to the rail camera controller - see InitializeCameras()
            //this.drivableBoxObject = new ModelObject("drivable box1", ActorType.Player, transform, effectParameters,
            //    this.modelDictionary["box2"]);

            ////attach a DriveController
            //drivableBoxObject.AttachController(new DriveController("driveController1", ControllerType.Drive,
            //    AppData.PlayerOneMoveKeys, AppData.PlayerMoveSpeed, AppData.PlayerStrafeSpeed, AppData.PlayerRotationSpeed, this.managerParameters));

            ////add to the objectManager so that it will be drawn and updated
            //this.objectManager.Add(drivableBoxObject);
        }

        //demo of some semi-transparent non-collidable ModelObjects
        private void InitializeNonCollidableDecoratorObjects()
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["ground_base"];
 

            //some things we can do with effect parameters
            //effectParameters.SpecularColor = Color.ForestGreen;
            //effectParameters.SpecularPower = 1;
            //effectParameters.EmissiveColor = Color.Brown;   
            //effectParameters.Alpha = 0.5f;

            Transform3D transform = new Transform3D(new Vector3(14, 0, -23), Vector3.One);

            ModelObject ground_base = new ModelObject("ground_base", ActorType.Decorator, transform, effectParameters,
                this.modelDictionary["ground_base"]);
            this.objectManager.Add(ground_base);


            #region Pillar Activation Decoration

            transform = new Transform3D(new Vector3(-3f, 0, -100),
                new Vector3(0, -160, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            BasicEffectParameters effectParameters2 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters2.Texture = this.textureDictionary["pillar_active"];
            effectParameters2.Alpha = 0f;

            effectParameters2.EmissiveColor = Color.White;
            //effectParameters.SpecularColor = Color.White;
            //effectParameters.SpecularPower = 1;
            pillarActivationDecoration1 = new ModelObject("pillar_A01", ActorType.Decorator, transform, effectParameters2, this.modelDictionary["P_A"]);
            //this.pillarActivationDecoration.Alpha = 0f;
            this.objectManager.Add(pillarActivationDecoration1);

            transform = new Transform3D(new Vector3(3.1f, 0, -126.1f),
                new Vector3(0, -160, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            effectParameters2 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters2.Texture = this.textureDictionary["pillar_active"];
            effectParameters2.Alpha = 0f;

            effectParameters2.EmissiveColor = Color.White;
            pillarActivationDecoration2 = new ModelObject("pillar_A02", ActorType.Decorator, transform,
                effectParameters2, this.modelDictionary["P_A"]);
            this.objectManager.Add(pillarActivationDecoration2);

            transform = new Transform3D(new Vector3(-25.1f, 0, -116.1f),
                new Vector3(0, -160, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            effectParameters2 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters2.Texture = this.textureDictionary["pillar_active"];
            effectParameters2.Alpha = 0f;

            effectParameters2.EmissiveColor = Color.White;
            pillarActivationDecoration3 = new ModelObject("pillar_A03", ActorType.Decorator, transform,
                effectParameters2, this.modelDictionary["P_A"]);

            this.objectManager.Add(pillarActivationDecoration3);

            #endregion

            //add more clones here...
        }

        private void InitializeBuildings()
        {
            //Transform3D transform3D = new Transform3D(new Vector3(-100, 0, 0),
            //    new Vector3(0, 90, 0), 0.4f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            //BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //effectParameters.Texture = this.textureDictionary["house-low-texture"];

            //CollidableObject collidableObject = new TriangleMeshObject("house1", ActorType.CollidableArchitecture, transform3D,
            //                    effectParameters, this.modelDictionary["house"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            //collidableObject.Enable(true, 1);
            //this.objectManager.Add(collidableObject);
        }

        private void InitializeWallsFences()
        {
            //Transform3D transform3D = new Transform3D(new Vector3(-140, 0, -14),
            //    new Vector3(0, -90, 0), 0.4f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            //BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //effectParameters.Texture = this.textureDictionary["wall"];

            //CollidableObject collidableObject = new TriangleMeshObject("wall1", ActorType.CollidableArchitecture, transform3D,
            //                effectParameters, this.modelDictionary["wall"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            //collidableObject.Enable(true, 1);
            //this.objectManager.Add(collidableObject);
        }

        private void InitializeVideoDisplay()
        {

            //BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //effectParameters.Texture = this.textureDictionary["checkerboard"];
            ////make the screen really shiny
            //effectParameters.SpecularPower = 256;

            ////put the display up on the Y-axis, obviously we can rotate by setting transform3D.Rotation
            //Transform3D transform3D = new Transform3D(new Vector3(0, 20, 0), new Vector3(16, 10, 0.01f));

            ///* 
            // * Does the display need to be collidable? if so use a CollidableObject and not a ModelObject.
            // * Notice we dont pass in a texture since the video controller will set this.
            // */
            //ModelObject videoModelObject = new ModelObject(AppData.VideoIDMainHall, ActorType.Decorator,
            //    transform3D, effectParameters, this.modelDictionary["box"]);

            //////create the controller
            //VideoController videoController = new VideoController(AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, ControllerType.Video, this.eventDispatcher,
            //    this.textureDictionary["checkerboard"], this.videoDictionary["Wildlife"], 0.5f);

            ////make it rotate like a commercial video display
            //videoModelObject.AttachController(new RotationController(AppData.VideoIDMainHall + " rotation " + AppData.ControllerIDSuffix,
            //    ControllerType.Rotation, new Vector3(0, 0.02f, 0)));
            ////attach
            //videoModelObject.AttachController(videoController);
            ////add to object manager
            //this.objectManager.Add(videoModelObject);
        }

   

        #region Initialize Cameras
        private void InitializeCamera(Integer2 screenResolution, string id, Viewport viewPort, Transform3D transform, 
            IController controller, float drawDepth)
        {
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardMediumFiveThree, viewPort, 1, StatusType.Update);

            if (controller != null)
                camera.AttachController(controller);

            this.cameraManager.Add(camera);
        }

        private void InitializeMultiScreenCameraDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "";
            int cameraHeight = 5;

            //non-collidable camera 1
            id = "non-collidable FPC 1";
            viewportDictionaryKey = "column1 row0";
            transform = new Transform3D(new Vector3(0, cameraHeight, 10), -Vector3.UnitZ, Vector3.UnitY);
            controller = new FirstPersonCameraController(id + " controller", ControllerType.FirstPerson, AppData.CameraMoveKeys, AppData.CameraMoveSpeed, AppData.CameraStrafeSpeed, AppData.CameraRotationSpeed,
                this.managerParameters);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 1
            id = "non-collidable security 1";
            viewportDictionaryKey = "column0 row0";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 60, AppData.SecurityCameraRotationSpeedSlow, AppData.SecurityCameraRotationAxisYaw);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 2
            id = "non-collidable security 2";
            viewportDictionaryKey = "column0 row1";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 45, AppData.SecurityCameraRotationSpeedMedium, new Vector3(1, 1, 0));
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 3
            id = "non-collidable security 3";
            viewportDictionaryKey = "column0 row2";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 30, AppData.SecurityCameraRotationSpeedFast, new Vector3(4, 1, 0));
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //track camera 1
            id = "non-collidable track 1";
            viewportDictionaryKey = "column0 row3";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            viewportDictionaryKey = "column0 row4";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;
            controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);
        }

        private void InitializeCollidableFirstPersonDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            string id = "";
            string viewportDictionaryKey = "";
            float drawDepth = 0;

            id = "collidable first person camera";
            viewportDictionaryKey = "full viewport";
            //doesnt matter how high on Y-axis we start the camera since it's collidable and will fall until the capsule toches the ground plane - see AppData::CollidableCameraViewHeight
            //just ensure that the Y-axis height is slightly more than AppData::CollidableCameraViewHeight otherwise the player will rise eerily upwards at the start of the game
            //as the CDCR system pushes the capsule out of the collidable ground plane 
            transform = new Transform3D(new Vector3(0, 1.1f * AppData.CollidableCameraViewHeight, 100), -Vector3.UnitZ, Vector3.UnitY);

            Camera3D camera = new Camera3D(id, ActorType.Camera, transform,
                    ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], drawDepth, StatusType.Update);

            //attach a CollidableFirstPersonController
            camera.AttachController(new CollidableFirstPersonCameraController(
                    camera + " controller",
                    ControllerType.CollidableFirstPerson,
                    AppData.CameraMoveKeys,
                    AppData.CollidableCameraMoveSpeed, AppData.CollidableCameraStrafeSpeed, AppData.CameraRotationSpeed,
                    this.managerParameters,
                    camera, //parent
                    AppData.CollidableCameraCapsuleRadius,
                    AppData.CollidableCameraViewHeight,
                    1, 1, //accel, decel
                    AppData.CollidableCameraMass,
                    AppData.CollidableCameraJumpHeight,
                    Vector3.Zero)); //translation offset

            this.cameraManager.Add(camera);
        }


        private void InitializeSingleScreenCameraDemo(Integer2 screenResolution)
        {
            InitializeCollidableFirstPersonDemo(screenResolution);

            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "non-collidable track 1";
            transform = new Transform3D(new Vector3(0, 0, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;

            //track animated player if it's available
            //if (this.animatedHeroPlayerObject != null)
            //    controller = new RailController(id + " controller", ControllerType.Rail, this.animatedHeroPlayerObject, this.railDictionary["rail1 - parallel to x-axis"]);
            //else
            controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);


            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);
        }

        //adds three camera from 3 different perspectives that we can cycle through
        private void InitializeSingleScreenCycleableCameraDemo(Integer2 screenResolution)
        {

            InitializeSingleScreenCameraDemo(screenResolution);

            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "non-collidable track 1";
            transform = new Transform3D(new Vector3(0, 0, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;
            
            controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

        }

        //adds a third person looking at collidable HeroPlayerObject
        private void InitializeCollidableThirdPersonDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "third person collidable";
            //doesnt matter since it will reset based on target actor data
            transform = Transform3D.Zero;

            if (this.animatedHeroPlayerObject != null)
            {
                controller = new ThirdPersonController(id + "ctrllr", ControllerType.ThirdPerson, this.animatedHeroPlayerObject,
                AppData.CameraThirdPersonDistance, AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                AppData.CameraThirdPersonElevationAngleInDegrees, AppData.CameraThirdPersonScrollSpeedElevatationMultiplier,
                LerpSpeed.Fast, LerpSpeed.Medium, this.mouseManager);
            }
            else
            {
                controller = new ThirdPersonController(id + "ctrllr", ControllerType.ThirdPerson, this.heroPlayerObject,
                AppData.CameraThirdPersonDistance, AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                AppData.CameraThirdPersonElevationAngleInDegrees, AppData.CameraThirdPersonScrollSpeedElevatationMultiplier,
                LerpSpeed.Fast, LerpSpeed.Medium, this.mouseManager);
            }


            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

        }
        #endregion

        #region Events

        private void InitializeEventDispatcher()
        {
            //initialize based on the expected number of events
            this.eventDispatcher = new EventDispatcher(this, 20);

            //Don't forget to add to the component list, otherwise EventDispatcher::Update will not get called
            Components.Add(this.eventDispatcher);
        }

        private void StartGame()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

            //publish an event to set the camera
            object[] additionalEventParamsB = { "collidable first person camera 1" };
            EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
        }
        #endregion

        #region Menu

        private void AddMenuElements()
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            Color color = Color.White;
            UIButtonObject uiButtonObject = null, clone = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 70;

            #region Main Menu
            sceneID = "main menu";

            //retrieve the background texture
            texture = this.textureDictionary["mainmenu"];
            //scale the texture to fit the entire screen
            Vector2 scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("mainmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, color, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add start button
            buttonID = "startbtn";
            buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 4.8f, 200);
            texture = this.textureDictionary["genericbtn"];
            transform = new Transform2D(position,
                0, new Vector2((1.5f) / 3f, (0.6f) / 2.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, color, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.Black, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);

            // add inventory button -clone the audio button then just reset texture, ids etc in all the clones

            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "inventorybtn";
            clone.Text = "Inventory";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            //change the texture blend color
            //clone.Color = Color.HotPink;
            this.menuManager.Add(sceneID, clone);

            //add audio button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "audiobtn";
            clone.Text = "Audio";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            //change the texture blend color
            //clone.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add controls button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "controlsbtn";
            clone.Text = "Controls";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            //change the texture blend color
            //clone.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "exitbtn";
            clone.Text = "Exit";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 4 * verticalBtnSeparation);
            //change the texture blend color
            clone.Color = Color.LightYellow;
            //store the original color since if we modify with a controller and need to reset
            clone.OriginalColor = clone.Color;
            //attach another controller on the exit button just to illustrate multi-controller approach
            clone.AttachController(new UIColorSineLerpController("colorSineLerpController", ControllerType.SineColorLerp,
                    new TrigonometricParameters(1, 0.4f, 0), color, color));
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Audio Menu
            sceneID = "audio menu";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, color, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));


            //add volume up button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "volumeUpbtn";
            clone.Text = "Volume Up";
            //change the texture blend color
            // clone.Color = Color.LightPink;
            this.menuManager.Add(sceneID, clone);

            //add volume down button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            clone.ID = "volumeDownbtn";
            clone.Text = "Volume Down";
            //change the texture blend color
            // clone.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            clone.ID = "volumeMutebtn";
            clone.Text = "Volume Mute";
            //change the texture blend color
            // clone.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            //  clone.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Controls Menu
            sceneID = "controls menu";

            //retrieve the controls menu background texture
            texture = this.textureDictionary["controlsmenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 6.5f * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            //  clone.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion
        }

        private void AddUIElements()
        {
            InitializeUIMousePointer();
            InitializeUIProgress();
            InitializeUIInventoryMenu();
        }

        private void InitializeUIMousePointer()
        {
            Texture2D texture = this.textureDictionary["reticuleDefault"];
            //show complete texture
            Microsoft.Xna.Framework.Rectangle sourceRectangle = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height);

            //listens for object picking events from the object picking manager
            UIPickingMouseObject myUIMouseObject = new UIPickingMouseObject("picking mouseObject",
                ActorType.UITexture,
                new Transform2D(Vector2.One),
                this.fontDictionary["mouse"],
                "",
                new Vector2(0, 40),
                texture,
                this.mouseManager,
                this.eventDispatcher);
            this.uiManagerMouse.Add(myUIMouseObject);
        }

        private void InitializeUIProgress()
        {
            float separation = 20; //spacing between progress bars

            Transform2D transform = null;
            Texture2D texture = null;
            UITextureObject textureObject = null;
            Vector2 position = Vector2.Zero;
            Vector2 scale = Vector2.Zero;
            float verticalOffset = 20;
            int startValue;

            texture = this.textureDictionary["progress_white"];
            scale = new Vector2(1, 0.75f);

            #region Player 1 Progress Bar
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f - texture.Width * scale.X - separation, verticalOffset);
            transform = new Transform2D(position, 0, scale,
                Vector2.Zero, /*new Vector2(texture.Width/2.0f, texture.Height/2.0f),*/
                new Integer2(texture.Width, texture.Height));

            textureObject = new UITextureObject(AppData.PlayerOneProgressID,
                    ActorType.UIDynamicTexture,
                    StatusType.Drawn | StatusType.Update,
                    transform, Color.AliceBlue,
                    SpriteEffects.None,
                    1,
                    texture);

            //add a controller which listens for pickupeventdata send when the player (or red box) collects the box on the left
            startValue = 3; //just a random number between 0 and max to demonstrate we can set initial progress value
            textureObject.AttachController(new UIProgressController(AppData.PlayerOneProgressControllerID, ControllerType.UIProgress, startValue, 10, this.eventDispatcher));
            this.uiManager.Add(textureObject);
            #endregion
        }




        private void InitializeEffects()
        {
            BasicEffect basicEffect = null;
            DualTextureEffect dualTextureEffect = null;
            Effect billboardEffect = null;


            #region Lit objects
            //create a BasicEffect and set the lighting conditions for all models that use this effect in their EffectParameters field

            basicEffect = new BasicEffect(graphics.GraphicsDevice);

            basicEffect.TextureEnabled = true;
            basicEffect.PreferPerPixelLighting = true;
            basicEffect.EnableDefaultLighting();
            this.effectDictionary.Add(AppData.LitModelsEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For Unlit objects
            //used for model objects that dont interact with lighting i.e. sky
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.LightingEnabled = false;
            this.effectDictionary.Add(AppData.UnlitModelsEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For dual texture objects
            dualTextureEffect = new DualTextureEffect(graphics.GraphicsDevice);
            this.effectDictionary.Add(AppData.UnlitModelDualEffectID, new DualTextureEffectParameters(dualTextureEffect));
            #endregion

            #region For unlit billboard objects
            billboardEffect = Content.Load<Effect>("Assets/Effects/Billboard");
            this.effectDictionary.Add(AppData.UnlitBillboardsEffectID, new BillboardEffectParameters(billboardEffect));
            #endregion

            #region For unlit primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = true;
            this.effectDictionary.Add(AppData.UnLitPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion
        }
        #endregion

        #region Content, Update, Draw

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
//            spriteBatch = new SpriteBatch(GraphicsDevice);

//            #region Add Menu and UI
//            InitializeMenu();
//            AddMenuElements();
//            InitializeUI();
//            AddUIElements();
//            #endregion

//#if DEBUG
//            InitializeDebugTextInfo();
//#endif

        }

        private void StatueRingIncrease(ModelObject statue)
        {
            float playerX = animatedHeroPlayerObject.Transform3D.Translation.X;
            float playerZ = animatedHeroPlayerObject.Transform3D.Translation.Z;
            float statueX;
            float statueZ;
            if (statue != null)
            {
                statueX = statue.Transform3D.Translation.X;
                statueZ = statue.Transform3D.Translation.Z;
            }
            else
            {
                statueX = 0;//statue.Transform3D.Translation.X;
                statueZ = 0;//statue.Transform3D.Translation.Z;
            }


            double distancePlayerToStatue = getDistance(playerX, statueX, playerZ, statueZ);

            RingIncrease(ringObject, distancePlayerToStatue, statueRadius);
        }

        private void RingIncrease(ModelObject model, double distance, double radius)
        {
            if (distance <= radius && model.Transform3D.Scale.X <= ringMaximum.X && this.inventoryManager.ItemUsed("Magical Sword"))
            {
                this.soundManager.ChangeVolumeRing(0.05F, "RingGrow");
                this.soundManager.PlaySound("RingGrow");
                model.Transform3D.Scale += new Vector3(ringSizeIncrease, 0, ringSizeIncrease);

            }
            else
            {
                if(model.Transform3D.Scale.X >= ringMinimum.X)
                {
                    model.Transform3D.Scale -= new Vector3(ringSizeDecrease, 0, ringSizeDecrease);
                }
                this.soundManager.ChangeVolumeRing(-0.02F, "RingGrow");
            }
        }

        private void ringUIChange()
        {
            if (ringObject.Transform3D.Scale.X > upperScale)
            {
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, (Integer)1 };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));

                lowerScale = ringObject.Transform3D.Scale.X;
                upperScale = ringObject.Transform3D.Scale.X + 0.2f;
            }
            
            if(ringObject.Transform3D.Scale.X < lowerScale)
            {
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, (Integer)(-1) };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));

                lowerScale = ringObject.Transform3D.Scale.X - 0.2f;
                upperScale = ringObject.Transform3D.Scale.X;
            }
            
        }

        private void pillarUpdate(ModelObject model)
        {
            float playerX = animatedHeroPlayerObject.Transform3D.Translation.X;
            float playerZ = animatedHeroPlayerObject.Transform3D.Translation.Z;
            float ringScale = ringObject.Transform3D.Scale.X;
            float radius = 13 * ringScale;

            float modelX = model.Transform3D.Translation.X;
            float modelZ = model.Transform3D.Translation.Z;

            double distancePlayerToPillar = getDistance(playerX, modelX, playerZ, modelZ);

           
            pillarGlow(model, distancePlayerToPillar, radius);
            playSound(model);

        }

        private void playSound(ModelObject model)
        {
            bool AlphaZero = true;
            int count = 0;
            for (int i = 0; i < 3; i++)
            {
                if(pillarAlphas[i] == 0)
                {
                    AlphaZero = false;
                }
                else if (pillarAlphas[i] > 0)
                {
                    count++;
                }
            }
            if(model.Alpha > 0 && !AlphaZero)
            {
                //object[] additionalParameters = { model.GetID() };
                //EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                this.soundManager.PlaySound(model.GetID());
            }

            if (count == 3)
            {
                if(!PillarEndPlayed)
                {
                    this.soundManager.PlaySound("PillarEnd");
                    PillarEndPlayed = true;
                }

            }

        }

        private double getDistance(float x, float x2, float z, float z2)
        {
            double distance = 0;

            distance = Math.Sqrt(Math.Pow(x - x2, 2) + Math.Pow(z - z2, 2));
            return distance;
        }

        private void pillarGlow(ModelObject model, double distance, float radius)
        {
            if (distance <= radius)
            {
                this.soundManager.ChangeVolumePillar(0.5F, model.GetID());
                if (model.Alpha > 0.99)// will alternate alpha to create a glowing effect
                {
                    this.isOpaque = true;
                }
                else if (model.Alpha < 0.4)
                {
                    this.isOpaque = false;
                }
                //this.ringObject.Transform3D.Scale += new Vector3(-0.001f, 0, -0.001f);
                if (this.isOpaque)
                {
                    changeAlpha(model, -0.01f);
                }
                else
                {
                    changeAlpha(model, 0.01f);
                }

            }
            else
            {
               
               this.soundManager.ChangeVolumePillar(-0.005F, model.GetID());

                //this.SoundPlayed = false;
                model.Alpha -= 0.05f;
            }


        }

        private void ringGlow(ModelObject model)
        {

            if (model.Alpha > 0.85)// will alternate alpha to create a glowing effect
            {
                this.isRingOpaque = true;
            }
            else if (model.Alpha < 0.4)
            {
                this.isRingOpaque = false;
            }
            //this.ringObject.Transform3D.Scale += new Vector3(-0.001f, 0, -0.001f);
            if (this.isRingOpaque)
            {
                changeAlpha(model, -0.005f);
            }
            else
            {
                changeAlpha(model, 0.005f);
            }
 
        }

        private void changeAlpha(ModelObject model, float alphaChange)
        {
            model.Alpha += alphaChange;
        }

        protected override void UnloadContent()
        {
            // formally call garbage collection to de-allocate resources from RAM
            this.modelDictionary.Dispose();
            this.textureDictionary.Dispose();
            this.fontDictionary.Dispose();
        }

        protected override void Update(GameTime gameTime)
        {
            //exit using new gamepad manager
            if (this.gamePadManager.IsPlayerConnected(PlayerIndex.One) && this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.Back))
                this.Exit();
            demoSoundManager();

#if DEMO
            #region Demo

            demoCameraChange();
            demoAlphaChange();
            demoUIProgressUpdate();
            demoHideDebugInfo();
            demoGamePadManager();
            demoVideoDisplay();
            #endregion
#endif
            this.pillarAlphas[0] = pillarActivationDecoration1.Alpha - 0.01f;
            this.pillarAlphas[1] = pillarActivationDecoration2.Alpha - 0.01f;
            this.pillarAlphas[2] = pillarActivationDecoration3.Alpha - 0.01f;

            pillarUpdate(pillarActivationDecoration1);

            pillarUpdate(pillarActivationDecoration2);

            pillarUpdate(pillarActivationDecoration3);

            StatueRingIncrease(Statue);

            ringUIChange();

            ringGlow(this.ringObject);

            base.Update(gameTime);
        }

#if DEMO
        #region DEMO
        
        private void demoGamePadManager()
        {
            if (this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightTrigger))
            {
                //do something....
            }
        }

        private void demoSoundManager()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.F2))
            {

                //object[] additionalParameters = {"boing"};
                //EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));


                //stopping a cue
                //Console.WriteLine("Key is pressed");
                //object[] additionalParameters = { "PillarLight", new Integer(0) };
                //EventDispatcher.Publish(new EventData(EventActionType.OnStop, EventCategoryType.Sound2D, additionalParameters));

            }
        }

        private void demoCameraChange()
        {
            //only single in single screen layout since cycling in multi-screen is meaningless
            if (this.screenManager.ScreenType == ScreenUtility.ScreenType.SingleScreen && this.keyboardManager.IsFirstKeyPress(Keys.C))
            {
                Console.WriteLine("Key Pressed");
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }
        }

        private void demoHideDebugInfo()
        {
            //show/hide debug info
            if (this.keyboardManager.IsFirstKeyPress(Keys.M))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnToggleDebug, EventCategoryType.Debug));
            }
        }

        private void demoUIProgressUpdate()
        {
            //testing event generation for UIProgressController
            if (this.keyboardManager.IsFirstKeyPress(Keys.O))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, new Integer(-1)};
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.L))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, new Integer(1) };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.I))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerTwoProgressControllerID, new Integer(-1)};
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.K))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerTwoProgressControllerID, new Integer(3) };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
        }

        private void demoAlphaChange()
        {
            if (this.drivableBoxObject != null)
            {
                //testing event generation on opacity change - see DrawnActor3D::Alpha setter
                if (this.keyboardManager.IsFirstKeyPress(Keys.F5))
                {
                    this.drivableBoxObject.Alpha -= 0.05f;
                }
                else if (this.keyboardManager.IsFirstKeyPress(Keys.F6))
                {
                    this.drivableBoxObject.Alpha += 0.05f;
                }
            }
        }

        private void demoVideoDisplay()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.Left))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Video, additonalParameters));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.Right))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix };
                EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.Video, additonalParameters));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.Up))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, new Integer(5) };
                EventDispatcher.Publish(new EventData(EventActionType.OnVolumeUp, EventCategoryType.Video, additonalParameters));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.Down))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, 0.05f };
                EventDispatcher.Publish(new EventData(EventActionType.OnMute, EventCategoryType.Video, additonalParameters));
            }


        }
        #endregion
#endif

        private void DrawDemoStatues()
        {

            BasicEffectParameters effectParametersStatue = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParametersStatue.Texture = this.textureDictionary["Statue"];

            Transform3D transform3D = new Transform3D(new Vector3(60, 0, 20), new Vector3(0, -65, -5), new Vector3(0.008f, 0.008f, 0.008f),
            Vector3.UnitX, Vector3.UnitY);

            TriangleMeshObject Acceptance = new TriangleMeshObject("Acceptance", ActorType.InteractiveStatue,
            transform3D, effectParametersStatue, this.modelDictionary["Statue"], this.modelDictionary["StatueLowPoly"],
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            Acceptance.Enable(true, 1);
            this.objectManager.Add(Acceptance);


            transform3D = new Transform3D(new Vector3(67.5f, 0, 9.5f), new Vector3(0, -65, 0), new Vector3(0.008f, 0.008f, 0.008f),
            Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParametersStatue2 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParametersStatue2.Texture = this.textureDictionary["Statue"];

            TriangleMeshObject Bargaining = new TriangleMeshObject("Bargaining", ActorType.InteractiveStatue,
            transform3D, effectParametersStatue2, this.modelDictionary["Statue"], this.modelDictionary["StatueLowPoly"],
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            Bargaining.Enable(true, 1);
            this.objectManager.Add(Bargaining);

            transform3D = new Transform3D(new Vector3(75, 0, -1), new Vector3(0, -65, 5), new Vector3(0.008f, 0.008f, 0.008f),
            Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParametersStatue3 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParametersStatue3.Texture = this.textureDictionary["Statue"];


            TriangleMeshObject Anger = new TriangleMeshObject("Anger", ActorType.InteractiveStatue,
            transform3D, effectParametersStatue3, this.modelDictionary["Statue"], this.modelDictionary["StatueLowPoly"],
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            Anger.Enable(true, 1);
            this.objectManager.Add(Anger);


            BasicEffectParameters effectParametersStatue4 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParametersStatue4.Texture = this.textureDictionary["Statue"];

            transform3D = new Transform3D(new Vector3(83.5f, 0, -11.5f), new Vector3(0, -65, 10), new Vector3(0.008f, 0.008f, 0.008f),
             Vector3.UnitX, Vector3.UnitY);

            TriangleMeshObject Denial = new TriangleMeshObject("Denial", ActorType.InteractiveStatue,
            transform3D, effectParametersStatue4, this.modelDictionary["Statue"], this.modelDictionary["StatueLowPoly"],
            new MaterialProperties(0.2f, 0.8f, 0.7f));
            Denial.Enable(true, 1);
            this.objectManager.Add(Denial);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
        }
        #endregion
    }
}


