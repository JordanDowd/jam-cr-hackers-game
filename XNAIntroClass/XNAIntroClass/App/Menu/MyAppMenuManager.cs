﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary.Managers.Menu;
using GDLibrary.Managers.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GDLibrary.Actors.Drawn2D.UI;
using GDLibrary.Enums;
using GDLibrary.Actors.Drawn._2D.UI;
using GDLibrary.Events.Base;
using GDLibrary.Managers.Camera;
using GDLibrary.Events.Data;

namespace XNAIntroClass.Menu
{
    public class MyAppMenuManager : MenuManager
    {
        public MyAppMenuManager(Game game, MouseManager mouseManager, KeyboardManager keyboardManager, CameraManager cameraManager,
            SpriteBatch spriteBatch, EventDispatcher eventDispatcher, StatusType statusType) :
            base(game, mouseManager, keyboardManager, cameraManager, spriteBatch, eventDispatcher, statusType)
        {

        }

        protected override void HandleMouseOver(UIObject currentUIObject)
        {
           
        }

        protected override void HandleMouseEntered(UIObject clickedUIObject)
        {
            object[] additionalParameters = { "ButtonHover" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
        }

        //add the code here to say how click events are handled by your code
        protected override void HandleMouseClick(UIObject clickedUIObject)
        {
            if (clickedUIObject.ActorType == ActorType.UIButton)
            {
                object[] additionalParameters = { "ButtonClick" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                //notice that the IDs are the same as the button IDs specified when we created the menu in Main::AddMenuElements()
                switch (clickedUIObject.ID)
                {
                    case "startbtn":
                        DoStart();
                        break;

                    case "exitbtn":
                        DoExit();
                        break;

                    case "audiobtn":
                        SetActiveList("audio menu"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                        break;

                    case "volumeUpbtn":
                        object[] VoumeUp = { 0.1f };
                        EventDispatcher.Publish(new EventData(EventActionType.OnVolumeChange, EventCategoryType.GlobalSound, VoumeUp));
                        break;

                    case "volumeDownbtn":
                        object[] VoumeDown = { -0.1f };
                        EventDispatcher.Publish(new EventData(EventActionType.OnVolumeChange, EventCategoryType.GlobalSound, VoumeDown));
                        break;

                    case "volumeMutebtn":
                        EventDispatcher.Publish(new EventData(EventActionType.OnMute, EventCategoryType.GlobalSound));
                        break;

                    case "backbtn":
                        SetActiveList("main menu"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                        break;

                    case "controlsbtn":
                        SetActiveList("controls menu"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                        break;

                    case "inventorybtn":
                        EventDispatcher.Publish(new EventData("ShowInventory", this, EventActionType.OnClick, EventCategoryType.Inventory));
                        SetActiveList("inventory list"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                        break;
                    default:
                        System.Diagnostics.Debug.WriteLine(clickedUIObject.ID);
                        break;
                }

            }
        }

        private void DoStart()
        {
            //will be received by the menu manager and the screenmanager
            //set game to be shown and set menu to be off
            EventDispatcher.Publish(new GDLibrary.Events.Data.EventData("MyAppMMDoStart", this,
                EventActionType.OnStart, EventCategoryType.MainMenu));

            object[] additionalParameters = { "GameMusic" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));

            object[] additionalParameters2 = { "Birds" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters2
                ));

        }

        private void DoExit()
        {
            this.Game.Exit();
            //to do - add exit Yes|No comfirmation
        }

    }
}

