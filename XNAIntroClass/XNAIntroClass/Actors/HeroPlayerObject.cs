﻿/* Represent your player. We can override the CollisionSkin_callbackFn() to define how the HeroPlayerObject reacts to collisions*/
using GDLibrary.Actors.Drawn._3D.Collidable;
using GDLibrary.Actors.Drawn._3D.Collidable.Player;
using GDLibrary.Enums;
using GDLibrary.Events.Base;
using GDLibrary.Events.Data;
using GDLibrary.Managers.Input;
using GDLibrary.Parameters.Other;
using GDLibrary.Parameters.Transforms;
using GDLibrary.Utility;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XNAIntroClass.Data;

namespace XNAIntroClass.App.Actors
{
    public class HeroPlayerObject : PlayerObject
    {
        private string progressControllerID;

        public HeroPlayerObject(string id, string progressControllerID, ActorType actorType, Transform3D transform,
            BasicEffect effect, ColorParameters colorParameters, Texture2D texture, Model model, Keys[] moveKeys, float radius,
            float height, float accelerationRate, float decelerationRate, float jumpHeight, Vector3 translationOffset,
            KeyboardManager keyboardManager)
            : base(id, actorType, transform, effect, colorParameters, texture, model, moveKeys, radius, height, accelerationRate,
                 decelerationRate, jumpHeight, translationOffset, keyboardManager)
        {
            //id of the progress controller associated with this player object - HandleCollision()
            this.progressControllerID = progressControllerID;
            //register for callback on CDCR
            this.CharacterBody.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
        }

        public object EventDispatacher { get; private set; }

        #region Event Handling
        public override bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            HandleCollisions(collider.Owner.ExternalData as CollidableObject, collidee.Owner.ExternalData as CollidableObject);
            return true;
        }

        //how do we want the object to respond to collisions
        private void HandleCollisions(CollidableObject collidableObjectCollider, CollidableObject collidableObjectCollidee)
        {
            //if (collidableObjectCollidee.ActorType == ActorType.CollidablePickup)
            //{
            //    //remove the object
            //    EventDispatcher.Publish(new EventData("removing bla", collidableObjectCollidee, EventActionType.OnRemoveActor,
            //        EventCategoryType.SystemRemove));
            //    //play a sound
            //    //decrement or incremenet score to the controller
            //    //additionalParameters only takes in reference types so we need to box the value into an object (Integer)
            //    object[] additionalEventParameters = { this.progressControllerID, (Integer)1 };
            //    EventDispatcher.Publish(new EventData(EventActionType.OnHealthChange, EventCategoryType.Player,
            //        additionalEventParameters));
            //}
        }
        #endregion

        protected override void HandleKeyboardInput(GameTime gameTime)
        {
            //jump
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[AppData.IndexMoveJump]))
            {
                this.CharacterBody.DoJump(1.0f);
            }
            //crouch
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[AppData.IndexMoveCrouch]))
            {
                this.CharacterBody.IsCrouching = !this.CharacterBody.IsCrouching;
            }

            //forward/backward
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[AppData.IndexMoveForward]))
            {
                this.CharacterBody.Velocity += this.Transform3D.Look * 1 * gameTime.ElapsedGameTime.Milliseconds;
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[AppData.IndexMoveBackward]))
            {
                this.CharacterBody.Velocity -= this.Transform3D.Look * 1 * gameTime.ElapsedGameTime.Milliseconds;
            }
            else //decelerate to zero when not pressed
            {
                this.CharacterBody.DesiredVelocity = Vector3.Zero;
            }

            //strafe left/right
            if (this.KeyboardManager.IsKeyDown(this.MoveKeys[AppData.IndexRotateLeft]))
            {
                this.Transform3D.RotateAroundYBy(0.1f * gameTime.ElapsedGameTime.Milliseconds);
            }
            else if (this.KeyboardManager.IsKeyDown(this.MoveKeys[AppData.IndexRotateRight]))
            {
                this.Transform3D.RotateAroundYBy(-0.1f * gameTime.ElapsedGameTime.Milliseconds);
            }
            else //decelerate to zero when not pressed
            {
                this.CharacterBody.DesiredVelocity = Vector3.Zero;
            }

            //update the camera position to reflect the collision skin position
            this.Transform3D.Translation = this.CharacterBody.Position;

        }


    }
}